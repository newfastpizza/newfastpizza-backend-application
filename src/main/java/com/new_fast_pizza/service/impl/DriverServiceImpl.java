package com.new_fast_pizza.service.impl;

import com.new_fast_pizza.exception.OrderException;
import com.new_fast_pizza.model.entity.Driver;
import com.new_fast_pizza.model.entity.Order;
import com.new_fast_pizza.model.entity.TownPoint;
import com.new_fast_pizza.model.enums.DriverStatus;
import com.new_fast_pizza.model.enums.OrderStatus;
import com.new_fast_pizza.repository.DriverRepository;
import com.new_fast_pizza.repository.OrderRepository;
import com.new_fast_pizza.service.DriverService;
import com.new_fast_pizza.service.TownService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

@Service
public class DriverServiceImpl implements DriverService {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TownService townService;

    @Override
    @Transactional
    public List<Driver> getAllDrivers() {
        LOG.debug("Get all drivers");
        return driverRepository.findByIsEnabledTrue();
    }

    @Override
    @Transactional
    public Driver getDriverById(String id) {
        LOG.debug("Get driver by id: {}", id);
        Driver driver = driverRepository.findByIdAndIsEnabledTrue(id).orElseThrow(() -> {
            LOG.debug("Can't find driver by id: {}", id);
            throw new NullPointerException("Not such driver");
        });
        LOG.debug("Driver was found: {}", driver);
        return driver;
    }

    @Override
    @Transactional
    public Driver updateDriverInfo(String id, String name, String phone) {
        Driver driver = getDriverById(id);
        driver.setName(name);
        driver.setPhone(phone);
        LOG.debug("Change driver info: {}", driver);
        return driverRepository.save(driver);
    }

    @Transactional
    @Override
    public Driver disableDriver(String id) {
        Driver driver = getDriverById(id);
        if (driver.getOrder() != null) {
            LOG.debug("Driver has active order: {}", driver.getOrder());
            throw new OrderException("Can't delete driver with active order id=" + driver.getOrder().getId());
        }
        driver.setCurAddress(null);
        driver.setStatus(DriverStatus.OUT_OF_OFFICE);
        driver.setIsEnabled(false);
        LOG.debug("Disable driver {}", driver);
        return driverRepository.save(driver);
    }

    @Override
    public TownPoint updatePosition(String driverId, String townPointId) {
        Driver driver = getDriverById(driverId);
        TownPoint townPoint = townService.getTownPointById(townPointId);
        LOG.debug("Update driver {} to town point {}", driver, townPoint);
        driver.setCurAddress(townPoint);
        return driverRepository.save(driver).getCurAddress();
    }

    @Override
    @Transactional
    public Driver updateStatus(String driverId, DriverStatus status) {
        Driver driver = getDriverById(driverId);
        LOG.debug("Update driver {} to status {}", driver, status);
        driver.setStatus(status);
        return driverRepository.save(driver);
    }

    @Async
    @Override
    public void findDriver(Order order) {
        LOG.debug("Start finding driver for order {}", order);
        try {
            while (!findDriverTransaction(order)) {
                Thread.sleep(60000);
            }
        } catch (InterruptedException e) {
            LOG.error("Can't sleep thread during finding drivers", e);
        } /*catch (NullPointerException e) {
            LOG.debug("Can't load order during finding driver {}", order.getId());
        }*/
    }

    @Override
    public void freeDriver(Driver driver) {
        LOG.debug("Free driver {}", driver);
        if (driver != null) {
            driver.setOrder(null);
            driver.setStatus(DriverStatus.FREE);
            driverRepository.save(driver);
        }
    }

    @Transactional
    private boolean findDriverTransaction(Order order) {
        LOG.debug("Find driver for order {}", order);
        //orderRepository.findById(order.getId()).orElseThrow(() -> new NullPointerException("Not such order"));
        List<Driver> freeDrivers = driverRepository.findByStatus(DriverStatus.FREE);
        TownPoint office = order.getOffice();
        if (freeDrivers.size() == 0) {
            LOG.debug("Can't find free drivers");
            return false;
        }
        Optional<TownPoint> pointOpt = freeDrivers.stream().map(Driver::getCurAddress).filter(Objects::nonNull).findAny();
        if (!pointOpt.isPresent()) {
            LOG.debug("Can't find drivers with position");
            return false;
        }
        Function<Driver, Integer> findDis = driver ->
                townService.getPath(office.getId(), driver.getCurAddress().getId()).getDistances().stream().mapToInt(i -> i).sum();
        Driver driver = freeDrivers.stream().reduce((Driver driver1, Driver driver2) ->
                findDis.apply(driver1) <= findDis.apply(driver2) ? driver1 : driver2).orElseThrow();
        LOG.debug("Driver was found {}", driver);
        order.setDriver(driver);
        order.setStatus(OrderStatus.IS_DELIVERED);
        driver.setOrder(order);
        driver.setStatus(DriverStatus.HAS_ORDER);
        driverRepository.save(driver);
        orderRepository.save(order);
        return true;
    }

}
