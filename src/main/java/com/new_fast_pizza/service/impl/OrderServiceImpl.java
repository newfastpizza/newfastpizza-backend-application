package com.new_fast_pizza.service.impl;

import com.new_fast_pizza.exception.OrderException;
import com.new_fast_pizza.model.entity.*;
import com.new_fast_pizza.model.enums.OrderStatus;
import com.new_fast_pizza.repository.OrderRepository;
import com.new_fast_pizza.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger LOG = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private ProductService productService;

    @Autowired
    private TownService townService;

    @Transactional
    @Override
    public List<Order> getAllOrders() {
        LOG.debug("Get all orders");
        return orderRepository.findAll();
    }

    @Transactional
    @Override
    public List<Order> getAllOrderByClient(String clientId) {
        LOG.debug("Get all orders by client id");
        return orderRepository.findByClient_id(clientId);
    }

    @Transactional
    @Override
    public List<Order> getAllOrderByDriver(String driverId) {
        LOG.debug("Get all orders by driver id");
        return orderRepository.findByDriver_id(driverId);
    }

    @Override
    @Transactional
    public Order createOrder(Map<String, Integer> productsIds, String townPointId, Integer flatNumber, String phone, String comment, String clientId) {
        LOG.debug("Create order");
        Order order = new Order();
        List<BacketItem> backetItems = productsIds.entrySet().stream()
                .map(entity -> BacketItem.builder()
                        .product(productService.getById(entity.getKey()))
                        .count(entity.getValue())
                        .build())
                .peek(backetItem -> backetItem.setPricePerItem(backetItem.getProduct().getPrice()))
                .collect(Collectors.toList());
        for (BacketItem backetItem : backetItems)
            backetItem.setOrder(order);
        Client client = (clientId != null) ? clientService.getClientById(clientId) : null;
        TownPoint townPoint = townService.getTownPointById(townPointId);
        order.setBacket(backetItems);
        order.setClient(client);
        order.setComment(comment);
        order.setDate(new Date());
        order.setFlatNumber(flatNumber);
        order.setPhone(phone);
        order.setStatus(OrderStatus.IS_PREPARED);
        order.setTownPoint(townPoint);
        order.setOffice(townService.getNearestOffice(townPoint));
        LOG.debug("Save order {}", order);
        order = orderRepository.save(order);
        driverService.findDriver(order);
        return order;
    }

    @Transactional
    @Override
    public Order getOrderById(String id) {
        LOG.debug("Get order by id {}", id);
        return orderRepository.findById(id).orElseThrow(() -> new NullPointerException("Not such order"));
    }

    @Transactional
    @Override
    public Order deleteOrderById(String id) {
        Order order = getOrderById(id);
        LOG.debug("Delete order {}", order);
        if (order.getDriver() != null)
            driverService.freeDriver(order.getDriver());
        orderRepository.delete(order);
        return order;
    }

    @Transactional
    @Override
    public Order finishOrder(String id) {
        Order order = getOrderById(id);
        LOG.debug("Finish order {}", order);
        if (order.getStatus() != OrderStatus.IS_DELIVERED) {
            LOG.debug("Can't finish order with status {}", order.getStatus());
            throw new OrderException("Can't finish order with status " + order.getStatus());
        }
        order.setStatus(OrderStatus.IS_FINISHED);
        Driver driver = order.getDriver();
        driver.setOrder(null);
        driverService.freeDriver(driver);
        return orderRepository.save(order);
    }

    @Transactional
    @Override
    public Order deliverOrder(String id) {
        Order order = getOrderById(id);
        LOG.debug("Deliver order {}", order);
        if (order.getStatus() != OrderStatus.IS_PREPARED) {
            LOG.debug("Can't deliver order with status {}", order.getStatus());
            throw new OrderException("Can't deliver order with status " + order.getStatus());
        }
        if (order.getDriver() == null) {
            LOG.debug("Can't deliver order without driver");
            throw new OrderException("Can't deliver order without driver");
        }
        order.setStatus(OrderStatus.IS_DELIVERED);
        return orderRepository.save(order);
    }

}
