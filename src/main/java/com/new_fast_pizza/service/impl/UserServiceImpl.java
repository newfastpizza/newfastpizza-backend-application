package com.new_fast_pizza.service.impl;

import com.new_fast_pizza.model.entity.User;
import com.new_fast_pizza.repository.UserRepository;
import com.new_fast_pizza.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        LOG.debug("Load user by username: {}", username);
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found, username=" + username));
        LOG.debug("User was loaded: {}", user);
        return user;
    }

    @Override
    public User getUserById(String id) {
        LOG.debug("Get user by id {}", id);
        User user = userRepository.findByIdAndIsEnabledTrue(id).orElseThrow(() -> {
            LOG.debug("Can't find user by id: {}", id);
            return new NullPointerException("Not such user");
        });
        LOG.debug("User was found: {}", user);
        return user;
    }

}
