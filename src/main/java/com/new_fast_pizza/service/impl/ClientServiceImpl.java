package com.new_fast_pizza.service.impl;

import com.new_fast_pizza.model.entity.Client;
import com.new_fast_pizza.repository.ClientRepository;
import com.new_fast_pizza.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private static final Logger LOG = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> getAllClients() {
        LOG.debug("Get all clients");
        return clientRepository.findByIsEnabledTrue();
    }

    @Override
    public Client getClientById(String id) {
        LOG.debug("Get client by id: {}", id);
        Client client = clientRepository.findByIdAndIsEnabledTrue(id).orElseThrow(() -> {
            LOG.debug("Can't find client by id = {}", id);
            return new NullPointerException("Not such client");
        });
        LOG.debug("Client was found: {}", client);
        return client;
    }

    @Override
    public Client updateClientInfo(String id, String name, String email, String phone) {
        Client client = getClientById(id);
        client.setName(name);
        client.setEmail(email);
        client.setPhone(phone);
        LOG.debug("Change client info: {}", client);
        return clientRepository.save(client);
    }

    @Override
    public Client disableClient(String id) {
        Client client = getClientById(id);
        client.setIsEnabled(false);
        LOG.debug("Disable client: {}", client);
        return clientRepository.save(client);
    }

}
