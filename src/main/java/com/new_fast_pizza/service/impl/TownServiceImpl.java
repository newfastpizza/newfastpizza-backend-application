package com.new_fast_pizza.service.impl;

import com.google.common.collect.Lists;
import com.new_fast_pizza.model.PointsPath;
import com.new_fast_pizza.model.entity.TownLink;
import com.new_fast_pizza.model.entity.TownPoint;
import com.new_fast_pizza.repository.TownLinkRepository;
import com.new_fast_pizza.repository.TownPointRepository;
import com.new_fast_pizza.service.TownService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;

@Service
public class TownServiceImpl implements TownService {

    private static final Logger LOG = LoggerFactory.getLogger(TownServiceImpl.class);

    @Autowired
    private TownPointRepository townPointRepository;

    @Autowired
    private TownLinkRepository townLinkRepository;

    @Override
    public TownPoint getNearestOffice(TownPoint townPoint) {
        LOG.debug("Find the nearest office from {}", townPoint);
        Function<TownPoint, Integer> findDis = point ->
                getPath(townPoint.getId(), point.getId()).getDistances().stream().mapToInt(i -> i).sum();
        List<TownPoint> offices = getAllOffices();
        return offices.stream().reduce((TownPoint office1, TownPoint office2) ->
                findDis.apply(office1) <= findDis.apply(office2) ? office1 : office2).orElseThrow();
    }

    @Override
    public List<TownPoint> getAllTownPoints() {
        LOG.debug("Get all town points");
        return townPointRepository.findAll();
    }

    @Override
    public List<TownPoint> getAllOffices() {
        LOG.debug("Get all offices");
        return townPointRepository.findAllByIsBranchOfficeTrue();
    }

    @Override
    public PointsPath getPath(String sourceId, String targetId) {
        TownPoint sourcePoint = getTownPointById(sourceId);
        TownPoint targetPoint = getTownPointById(targetId);
        List<TownPoint> townPoints = getAllTownPoints();
        List<TownLink> townLinks = townLinkRepository.findAll();
        LOG.debug("Find shortest path from {} to {}", sourcePoint, targetPoint);
        Map<TownPoint, TownLink> previousLinks = new HashMap<>();
        Map<TownPoint, Integer> distances = new HashMap<>();
        distances.put(sourcePoint, 0);
        Set<TownPoint> queuePoints = new TreeSet<>(new Comparator<>() {
            @Override
            public int compare(TownPoint o1, TownPoint o2) {
                Integer val1 = distances.getOrDefault(o1, Integer.MAX_VALUE);
                Integer val2 = distances.getOrDefault(o2, Integer.MAX_VALUE);
                return (!val1.equals(val2)) ? Integer.compare(val1, val2) : o1.getId().compareTo(o2.getId());
            }
        });
        queuePoints.add(sourcePoint);
        while (!queuePoints.isEmpty()) {
            TownPoint curPoint = queuePoints.iterator().next();
            queuePoints.remove(curPoint);
            if (curPoint.getId().equals(targetId))
                break;
            townLinks.forEach(townLink -> {
                if (curPoint.equals(townLink.getSourceTownPoint())) {
                    TownPoint nextPoint = townLink.getTargetTownPoint();
                    if (distances.getOrDefault(nextPoint, Integer.MAX_VALUE) > distances.get(curPoint) + townLink.getDuration()) {
                        queuePoints.remove(nextPoint);
                        previousLinks.put(nextPoint, townLink);
                        distances.put(nextPoint, distances.get(curPoint) + townLink.getDuration());
                        queuePoints.add(nextPoint);
                    }
                }
            });
        }
        List<TownPoint> path = new ArrayList<>();
        List<Integer> durations = new ArrayList<>();
        TownPoint curPoint = targetPoint;
        path.add(curPoint);
        while (true) {
            TownLink link = previousLinks.get(curPoint);
            if (link == null)
                break;
            curPoint = link.getSourceTownPoint();
            path.add(curPoint);
            durations.add(link.getDuration());
        }
        return PointsPath.builder()
                .path(Lists.reverse(path))
                .distances(Lists.reverse(durations))
                .build();
    }

    @Override
    public TownPoint getTownPointById(String id) {
        LOG.debug("Get town point by id {}", id);
        return townPointRepository.findById(id).orElseThrow(() -> new NullPointerException("Not such town point"));
    }

    @Override
    public TownPoint setAsOffice(String id, Boolean isOffice) {
        TownPoint townPoint = getTownPointById(id);
        LOG.debug("Set town point {} as branch office {}", townPoint, isOffice);
        townPoint.setIsBranchOffice(isOffice);
        return townPointRepository.save(townPoint);
    }

    @Override
    public TownPoint getPointByAddress(String street, String building) {
        LOG.debug("Get town point by address {} {}", street, building);
        return townPointRepository.findByStreetAndBuilding(street, building)
                .orElseThrow(() -> new NullPointerException("Not such town point"));
    }


}
