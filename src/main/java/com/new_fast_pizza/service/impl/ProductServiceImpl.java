package com.new_fast_pizza.service.impl;

import com.new_fast_pizza.model.entity.Drink;
import com.new_fast_pizza.model.entity.Pizza;
import com.new_fast_pizza.model.entity.Product;
import com.new_fast_pizza.repository.DrinkRepository;
import com.new_fast_pizza.repository.PizzaRepository;
import com.new_fast_pizza.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ProductServiceImpl implements ProductService {

    private static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    private PizzaRepository pizzaRepository;

    @Autowired
    private DrinkRepository drinkRepository;

    @Transactional
    @Override
    public List<Product> getAllProducts() {
        LOG.debug("Get all products");
        return Stream.concat(getAllPizzas().stream(), getAllDrinks().stream())
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<Pizza> getAllPizzas() {
        LOG.debug("Get all pizzas");
        return pizzaRepository.findByIsEnabledTrue();
    }

    @Transactional
    @Override
    public List<Drink> getAllDrinks() {
        LOG.debug("Get all drinks");
        return drinkRepository.findByIsEnabledTrue();
    }

    @Override
    public Pizza updatePizza(Pizza pizza) {
        LOG.debug("Update pizza {}", pizza);
        return pizzaRepository.save(pizza);
    }

    @Override
    public Drink updateDrink(Drink drink) {
        LOG.debug("Update drink {}", drink);
        return drinkRepository.save(drink);
    }

    @Override
    public Product getById(String id) {
        LOG.debug("Get product by id: {}", id);
        Optional<Pizza> pizza = pizzaRepository.findByIdAndIsEnabledTrue(id);
        Optional<Drink> drink = drinkRepository.findByIdAndIsEnabledTrue(id);
        if (pizza.isPresent())
            return pizza.get();
        else if (drink.isPresent())
            return drink.get();
        else {
            LOG.debug("Can't find product by id: {}", id);
            throw new NullPointerException("Can't find product with id=" + id);
        }
    }

    @Override
    public Product deleteById(String id) {
        Product product = getById(id);
        product.setIsEnabled(false);
        if (product instanceof Pizza) {
            LOG.debug("Disable pizza {}", product);
            return pizzaRepository.save((Pizza) product);
        } else {
            LOG.debug("Disable drink {}", product);
            return drinkRepository.save((Drink) product);
        }
    }
}
