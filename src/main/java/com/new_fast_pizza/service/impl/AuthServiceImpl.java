package com.new_fast_pizza.service.impl;

import com.new_fast_pizza.component.security.JwtComponent;
import com.new_fast_pizza.exception.SignUpException;
import com.new_fast_pizza.model.entity.Client;
import com.new_fast_pizza.model.entity.Driver;
import com.new_fast_pizza.model.entity.Role;
import com.new_fast_pizza.model.entity.User;
import com.new_fast_pizza.model.enums.DriverStatus;
import com.new_fast_pizza.model.enums.RoleType;
import com.new_fast_pizza.repository.RoleRepository;
import com.new_fast_pizza.repository.UserRepository;
import com.new_fast_pizza.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AuthServiceImpl implements AuthService {

    private static final Logger LOG = LoggerFactory.getLogger(AuthServiceImpl.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtComponent jwtComponent;

    @Override
    public String authenticateUser(String username, String password) {
        LOG.debug("Sign in: username={}, password={}", username, password);
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return jwtComponent.generateJwtToken(authentication);
    }

    @Override
    public void registClient(String username, String name, String email, String phone, Date birthday, String password) throws SignUpException {
        LOG.debug("Client's registration: username={}, name={}, email={}, phone={}, birthday={}, password={}",
                username, name, email, phone, birthday, password);
        if (userRepository.existsByUsername(username)) {
            LOG.debug("User with username={} already exists", username);
            throw new SignUpException("User with username=" + username + " already exists");
        }
        Role clientRole = roleRepository.findRoleByRoleType(RoleType.ROLE_CLIENT).orElseThrow(() -> {
            LOG.error("Can't find client role");
            return new NullPointerException("Can't find client role");
        });
        Client client = Client.builder()
                .username(username)
                .name(name)
                .email(email)
                .phone(phone)
                .birthday(birthday)
                .password(encoder.encode(password))
                .role(clientRole)
                .build();
        userRepository.save(client);
    }

    @Override
    public void registDriver(String username, String name, String phone, String password) throws SignUpException {
        LOG.debug("Driver's registration: username={}, name={}, phone={}, password={}", username, name, phone, password);
        if (userRepository.existsByUsername(username)) {
            LOG.debug("User with username={} already exists", username);
            throw new SignUpException("User with username=" + username + " already exists");
        }
        Role driverRole = roleRepository.findRoleByRoleType(RoleType.ROLE_DRIVER).orElseThrow(() -> {
            LOG.error("Can't find driver role");
            return new NullPointerException("Can't find driver role");
        });
        Driver driver = Driver.builder()
                .username(username)
                .name(name)
                .phone(phone)
                .password(encoder.encode(password))
                .role(driverRole)
                .status(DriverStatus.OUT_OF_OFFICE)
                .build();
        userRepository.save(driver);
    }

    @Override
    public User getCurrentUser() {
        LOG.debug("Get current user");
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!user.isEnabled()) {
            LOG.debug("Current User Is Disabled");
            throw new SignUpException("User is disabled");
        }
        return user;
    }

    @Override
    public void changePassword(String oldPassword, String password) {
        LOG.debug("Change password {} -> {}", oldPassword, password);
        User user = getCurrentUser();
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getUsername(), oldPassword);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        user.setPassword(encoder.encode(password));
        userRepository.save(user);
        LOG.debug("Password was changed");
    }

}
