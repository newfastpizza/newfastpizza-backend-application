package com.new_fast_pizza.service;

import com.new_fast_pizza.model.entity.Client;

import java.util.List;

public interface ClientService {
    List<Client> getAllClients();

    Client getClientById(String id);

    Client updateClientInfo(String id, String name, String email, String phone);

    Client disableClient(String id);
}
