package com.new_fast_pizza.service;

import com.new_fast_pizza.exception.SignUpException;
import com.new_fast_pizza.model.entity.User;
import org.springframework.security.core.AuthenticationException;

import java.util.Date;

public interface AuthService {
    String authenticateUser(String username, String password) throws AuthenticationException;

    void registClient(String username, String name, String email, String phone, Date birthday, String password) throws SignUpException;

    void registDriver(String username, String name, String phone, String password) throws SignUpException;

    User getCurrentUser();

    void changePassword(String oldPassword, String password);
}
