package com.new_fast_pizza.service;

import com.new_fast_pizza.model.entity.Drink;
import com.new_fast_pizza.model.entity.Pizza;
import com.new_fast_pizza.model.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();

    List<Pizza> getAllPizzas();

    List<Drink> getAllDrinks();

    Pizza updatePizza(Pizza pizza);

    Drink updateDrink(Drink drink);

    Product getById(String id);

    Product deleteById(String id);
}
