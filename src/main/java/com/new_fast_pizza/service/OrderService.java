package com.new_fast_pizza.service;

import com.new_fast_pizza.model.entity.Order;

import java.util.List;
import java.util.Map;

public interface OrderService {

    List<Order> getAllOrders();

    List<Order> getAllOrderByClient(String clientId);

    List<Order> getAllOrderByDriver(String driverId);

    Order createOrder(Map<String, Integer> productsIds, String townPointId, Integer flatNumber, String phone, String comment, String clientId);

    Order getOrderById(String id);

    Order deleteOrderById(String id);

    Order finishOrder(String id);

    Order deliverOrder(String id);

}
