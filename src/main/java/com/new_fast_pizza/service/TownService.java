package com.new_fast_pizza.service;

import com.new_fast_pizza.model.PointsPath;
import com.new_fast_pizza.model.entity.TownPoint;

import java.util.List;

public interface TownService {

    TownPoint getNearestOffice(TownPoint townPoint);

    List<TownPoint> getAllTownPoints();

    List<TownPoint> getAllOffices();

    PointsPath getPath(String sourceId, String targetId);

    TownPoint getTownPointById(String id);

    TownPoint setAsOffice(String id, Boolean isOffice);

    TownPoint getPointByAddress(String street, String building);
}
