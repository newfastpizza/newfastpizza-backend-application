package com.new_fast_pizza.service;

import com.new_fast_pizza.model.entity.Driver;
import com.new_fast_pizza.model.entity.Order;
import com.new_fast_pizza.model.entity.TownPoint;
import com.new_fast_pizza.model.enums.DriverStatus;

import java.util.List;

public interface DriverService {
    List<Driver> getAllDrivers();

    Driver getDriverById(String id);

    Driver updateDriverInfo(String id, String name, String phone);

    Driver disableDriver(String id);

    TownPoint updatePosition(String driverId, String townPointId);

    Driver updateStatus(String driverId, DriverStatus status);

    void findDriver(Order order);

    void freeDriver(Driver driver);
}
