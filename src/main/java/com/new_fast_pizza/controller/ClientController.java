package com.new_fast_pizza.controller;

import com.new_fast_pizza.model.ChangeClientProfileRequest;
import com.new_fast_pizza.model.entity.Client;
import com.new_fast_pizza.model.entity.User;
import com.new_fast_pizza.model.enums.RoleType;
import com.new_fast_pizza.service.AuthService;
import com.new_fast_pizza.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/clients")
@Validated
public class ClientController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private AuthService authService;

    @GetMapping(value = "/profile", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_CLIENT')")
    public ResponseEntity<Client> getOwnProfile() {
        Client client = (Client) authService.getCurrentUser();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(client);
    }

    @GetMapping(produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<Client>> getClients() {
        List<Client> clients = clientService.getAllClients();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(clients);
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    public ResponseEntity<Client> getClientById(@PathVariable String id) {
        User curUser = authService.getCurrentUser();
        if (curUser.getRole().getRoleType().equals(RoleType.ROLE_CLIENT) && curUser.getId().equals(id)) {
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body((Client) curUser);
        } else if (curUser.getRole().getRoleType().equals(RoleType.ROLE_ADMIN)) {
            try {
                Client client = clientService.getClientById(id);
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(client);
            } catch (NullPointerException e) {
                return ResponseEntity.notFound().build();
            }
        } else
            return ResponseEntity.status(403).build();
    }

    @PostMapping(value = "/profile", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_CLIENT')")
    public ResponseEntity<Client> updateClientInfo(@Valid @RequestBody ChangeClientProfileRequest changeClientProfileRequest) {
        User curUser = authService.getCurrentUser();
        try {
            Client client = clientService.updateClientInfo(curUser.getId(), changeClientProfileRequest.getName(),
                    changeClientProfileRequest.getEmail(), changeClientProfileRequest.getPhone());
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(client);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "/profile", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_CLIENT')")
    public ResponseEntity<Client> deleteClient() {
        User curUser = authService.getCurrentUser();
        try {
            Client client = clientService.disableClient(curUser.getId());
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(client);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }

}
