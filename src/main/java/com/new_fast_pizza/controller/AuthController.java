package com.new_fast_pizza.controller;

import com.new_fast_pizza.exception.SignUpException;
import com.new_fast_pizza.model.ChangePasswordRequest;
import com.new_fast_pizza.model.LoginRequest;
import com.new_fast_pizza.model.SignUpClientRequest;
import com.new_fast_pizza.model.SignUpDriverRequest;
import com.new_fast_pizza.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/auth")
@Validated
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/signin")
    public ResponseEntity<String> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        String jwt = authService.authenticateUser(loginRequest.getUsername(), loginRequest.getPassword());
        return ResponseEntity.ok(jwt);
    }

    @PostMapping("/signup/client")
    public ResponseEntity<String> registClient(@Valid @RequestBody SignUpClientRequest signUpClientRequest) {
        if (!signUpClientRequest.getPassword().equals(signUpClientRequest.getConfirmPassword()))
            return ResponseEntity.badRequest().body("Need to confirm the password");
        try {
            authService.registClient(signUpClientRequest.getUsername(),
                    signUpClientRequest.getName(),
                    signUpClientRequest.getEmail(),
                    signUpClientRequest.getPhone(),
                    signUpClientRequest.getBirthday(),
                    signUpClientRequest.getPassword());
        } catch (SignUpException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body("Client registered successfully!");
    }

    @PostMapping("/signup/driver")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<String> registDriver(@Valid @RequestBody SignUpDriverRequest signUpDriverRequest) {
        if (!signUpDriverRequest.getPassword().equals(signUpDriverRequest.getConfirmPassword()))
            return ResponseEntity.badRequest().body("Need to confirm the password");
        try {
            authService.registDriver(signUpDriverRequest.getUsername(),
                    signUpDriverRequest.getName(),
                    signUpDriverRequest.getPhone(),
                    signUpDriverRequest.getPassword());
        } catch (SignUpException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body("Driver registered successfully!");
    }

    @PostMapping("/password")
    @PreAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_DRIVER')")
    public ResponseEntity<String> changePassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {
        if (!changePasswordRequest.getPassword().equals(changePasswordRequest.getConfirmPassword()))
            return ResponseEntity.badRequest().body("Need to confirm the password");
        try {
            authService.changePassword(changePasswordRequest.getOldPassword(), changePasswordRequest.getPassword());
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body("Password was changed successfully!");
    }

}
