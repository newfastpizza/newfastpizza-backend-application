package com.new_fast_pizza.controller;

import com.new_fast_pizza.exception.OrderException;
import com.new_fast_pizza.model.ChangeDriverProfileRequest;
import com.new_fast_pizza.model.entity.Driver;
import com.new_fast_pizza.model.entity.Order;
import com.new_fast_pizza.model.entity.TownPoint;
import com.new_fast_pizza.model.entity.User;
import com.new_fast_pizza.model.enums.DriverStatus;
import com.new_fast_pizza.model.enums.RoleType;
import com.new_fast_pizza.service.AuthService;
import com.new_fast_pizza.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/drivers")
@Validated
public class DriverController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private AuthService authService;

    @GetMapping(value = "/profile", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_DRIVER')")
    public ResponseEntity<Driver> getOwnProfile() {
        Driver driver = (Driver) authService.getCurrentUser();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(driver);
    }

    @GetMapping(produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<Driver>> getDrivers() {
        List<Driver> drivers = driverService.getAllDrivers();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(drivers);
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DRIVER')")
    public ResponseEntity<Driver> getDriverById(@PathVariable String id) {
        User curUser = authService.getCurrentUser();
        if (curUser.getRole().getRoleType().equals(RoleType.ROLE_DRIVER) && curUser.getId().equals(id)) {
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body((Driver) curUser);
        } else if (curUser.getRole().getRoleType().equals(RoleType.ROLE_ADMIN)) {
            try {
                Driver driver = driverService.getDriverById(id);
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(driver);
            } catch (NullPointerException e) {
                return ResponseEntity.notFound().build();
            }
        } else
            return ResponseEntity.status(403).build();
    }

    @PostMapping(value = "/{id}", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Driver> updateDriverInfo(@PathVariable String id,
                                                   @Valid @RequestBody ChangeDriverProfileRequest changeDriverProfileRequest) {
        try {
            Driver driver = driverService.updateDriverInfo(id, changeDriverProfileRequest.getName(), changeDriverProfileRequest.getPhone());
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(driver);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Driver> deleteDriver(@PathVariable String id) {
        try {
            Driver driver = driverService.disableDriver(id);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(driver);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        } catch (OrderException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping(value = "/{id}/order", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DRIVER')")
    public ResponseEntity<Order> getOrderForDriverById(@PathVariable String id) {
        User user = authService.getCurrentUser();
        if (user.getRole().getRoleType() == RoleType.ROLE_ADMIN || user.getId().equals(id)) {
            try {
                Driver driver = driverService.getDriverById(id);
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(driver.getOrder());
            } catch (NullPointerException e) {
                return ResponseEntity.notFound().build();
            }
        } else {
            return ResponseEntity.status(403).build();
        }
    }

    @GetMapping(value = "/{id}/position", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DRIVER')")
    public ResponseEntity<TownPoint> getPositionForDriverById(@PathVariable String id) {
        User user = authService.getCurrentUser();
        if (user.getRole().getRoleType() == RoleType.ROLE_ADMIN || user.getId().equals(id)) {
            try {
                Driver driver = driverService.getDriverById(id);
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(driver.getCurAddress());
            } catch (NullPointerException e) {
                return ResponseEntity.notFound().build();
            }
        } else {
            return ResponseEntity.status(403).build();
        }
    }

    @PostMapping(value = "/{id}/position", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DRIVER')")
    public ResponseEntity<TownPoint> updatePositionForDriverById(@PathVariable String id, @RequestBody String positionId) {
        User user = authService.getCurrentUser();
        if (user.getRole().getRoleType() == RoleType.ROLE_ADMIN || user.getId().equals(id)) {
            try {
                TownPoint townPoint = driverService.updatePosition(id, positionId);
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(townPoint);
            } catch (NullPointerException e) {
                return ResponseEntity.notFound().build();
            }
        } else {
            return ResponseEntity.status(403).build();
        }
    }

    @PostMapping(value = "/{id}/status", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_DRIVER')")
    public ResponseEntity<Driver> updateStatusForDriverById(@PathVariable String id, @RequestBody DriverStatus status) {
        User user = authService.getCurrentUser();
        if (user.getRole().getRoleType() == RoleType.ROLE_ADMIN || user.getId().equals(id)) {
            try {
                Driver driver = driverService.updateStatus(id, status);
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(driver);
            } catch (NullPointerException e) {
                return ResponseEntity.notFound().build();
            }
        } else {
            return ResponseEntity.status(403).build();
        }
    }
}
