package com.new_fast_pizza.controller;

import com.new_fast_pizza.model.entity.Drink;
import com.new_fast_pizza.model.entity.Pizza;
import com.new_fast_pizza.model.entity.Product;
import com.new_fast_pizza.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/products")
@Validated
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Product>> getAllProducts() {
        List<Product> products = productService.getAllProducts();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(products);
    }

    @GetMapping(value = "/pizzas", produces = "application/json")
    public ResponseEntity<List<Pizza>> getAllPizzas() {
        List<Pizza> pizzas = productService.getAllPizzas();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(pizzas);
    }

    @PostMapping(value = "/pizzas", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Pizza> updatePizza(@Valid @RequestBody Pizza pizza) {
        Pizza savePizza = productService.updatePizza(pizza);
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(savePizza);
    }

    @GetMapping(value = "/drinks", produces = "application/json")
    public ResponseEntity<List<Drink>> getAllDrinks() {
        List<Drink> drinks = productService.getAllDrinks();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(drinks);
    }

    @PostMapping(value = "/drinks", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Drink> updateDrink(@Valid @RequestBody Drink drink) {
        Drink saveDrink = productService.updateDrink(drink);
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(saveDrink);
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Product> getProductById(@PathVariable String id) {
        try {
            Product product = productService.getById(id);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(product);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Product> deleteProductById(@PathVariable String id) {
        try {
            Product product = productService.deleteById(id);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(product);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }


}
