package com.new_fast_pizza.controller;

import com.new_fast_pizza.model.PointsPath;
import com.new_fast_pizza.model.entity.TownPoint;
import com.new_fast_pizza.service.TownService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/town")
@Validated
public class TownController {

    @Autowired
    private TownService townService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<TownPoint>> getAllTownPoints() {
        List<TownPoint> townPoints = townService.getAllTownPoints();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(townPoints);
    }

    @GetMapping(value = "/offices", produces = "application/json")
    public ResponseEntity<List<TownPoint>> getAllOffices() {
        List<TownPoint> offices = townService.getAllOffices();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(offices);
    }

    @GetMapping(value = "/path/{sourceId}/{targetId}", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_DRIVER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<PointsPath> getShortestPath(@PathVariable String sourceId, @PathVariable String targetId) {
        try {
            PointsPath pointsPath = townService.getPath(sourceId, targetId);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(pointsPath);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<TownPoint> getTownPointById(@PathVariable String id) {
        try {
            TownPoint townPoint = townService.getTownPointById(id);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(townPoint);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "/{street}/{building}", produces = "application/json")
    public ResponseEntity<TownPoint> getTownByAddress(@PathVariable String street, @PathVariable String building) {
        try {
            TownPoint townPoint = townService.getPointByAddress(street, building);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(townPoint);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(value = "/{id}", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<TownPoint> setOfficeStatus(@PathVariable String id, @RequestBody Boolean isOffice) {
        try {
            TownPoint townPoint = townService.setAsOffice(id, isOffice);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(townPoint);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
