package com.new_fast_pizza.controller;

import com.new_fast_pizza.exception.OrderException;
import com.new_fast_pizza.model.CreateOrderRequest;
import com.new_fast_pizza.model.entity.Client;
import com.new_fast_pizza.model.entity.Driver;
import com.new_fast_pizza.model.entity.Order;
import com.new_fast_pizza.model.entity.User;
import com.new_fast_pizza.model.enums.RoleType;
import com.new_fast_pizza.service.AuthService;
import com.new_fast_pizza.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/orders")
@Validated
public class OrderController {

    @Autowired
    private AuthService authService;

    @Autowired
    private OrderService orderService;

    @GetMapping(produces = "application/json")
    @PreAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_DRIVER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<Order>> getOrders() {
        User curUser = authService.getCurrentUser();
        List<Order> orders = (curUser.getRole().getRoleType() == RoleType.ROLE_CLIENT) ?
                orderService.getAllOrderByClient(curUser.getId()) :
                (curUser.getRole().getRoleType() == RoleType.ROLE_DRIVER) ?
                        orderService.getAllOrderByDriver(curUser.getId()) :
                        orderService.getAllOrders();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(orders);
    }

    @PostMapping(produces = "application/json")
    public ResponseEntity<Order> createOrder(@Valid @RequestBody CreateOrderRequest createOrderRequest) {
        String clientId = null;
        try {
            User curUser = authService.getCurrentUser();
            if (curUser.getRole().getRoleType() == RoleType.ROLE_CLIENT)
                clientId = curUser.getId();
        } catch (ClassCastException ignored) {
        }
        try {
            Order order = orderService.createOrder(createOrderRequest.getProductsIds(),
                    createOrderRequest.getTownPointId(),
                    createOrderRequest.getFlatNumber(),
                    createOrderRequest.getPhone(),
                    createOrderRequest.getComment(),
                    clientId);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(order);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "{id}", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_DRIVER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<Order> getOrderById(@PathVariable String id) {
        User user = authService.getCurrentUser();
        try {
            Order order = orderService.getOrderById(id);
            if (user.getRole().getRoleType() == RoleType.ROLE_CLIENT && order.getClient().getId().equals(user.getId()) ||
                    user.getRole().getRoleType() == RoleType.ROLE_DRIVER && order.getDriver().getId().equals(user.getId()) ||
                    user.getRole().getRoleType() == RoleType.ROLE_ADMIN) {
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(order);
            } else
                return ResponseEntity.status(403).build();
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "{id}", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Order> deleteOrderById(@PathVariable String id) {
        try {
            Order order = orderService.deleteOrderById(id);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(order);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "{id}/finish", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_DRIVER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<Order> finishOrder(@PathVariable String id) {
        User user = authService.getCurrentUser();
        try {
            Order order = orderService.getOrderById(id);
            if (user.getRole().getRoleType() == RoleType.ROLE_DRIVER && order.getDriver().getId().equals(user.getId()) ||
                    user.getRole().getRoleType() == RoleType.ROLE_ADMIN) {
                Order saveOrder = orderService.finishOrder(id);
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(saveOrder);
            } else
                return ResponseEntity.status(403).build();
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        } catch (OrderException e) {
            return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).build();
        }
    }

    @GetMapping(value = "{id}/deliver", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_DRIVER') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<Order> deliverOrder(@PathVariable String id) {
        User user = authService.getCurrentUser();
        try {
            Order order = orderService.getOrderById(id);
            if (user.getRole().getRoleType() == RoleType.ROLE_DRIVER && order.getDriver().getId().equals(user.getId()) ||
                    user.getRole().getRoleType() == RoleType.ROLE_ADMIN) {
                Order saveOrder = orderService.deliverOrder(id);
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(saveOrder);
            } else
                return ResponseEntity.status(403).build();
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        } catch (OrderException e) {
            return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).build();
        }
    }

    @GetMapping(value = "{id}/client", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Client> getClientForOrderById(@PathVariable String id) {
        try {
            Order order = orderService.getOrderById(id);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(order.getClient());
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "{id}/driver", produces = "application/json")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Driver> getDriverForOrderById(@PathVariable String id) {
        try {
            Order order = orderService.getOrderById(id);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(order.getDriver());
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
