package com.new_fast_pizza.component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.new_fast_pizza.model.entity.*;
import com.new_fast_pizza.model.enums.RoleType;
import com.new_fast_pizza.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(StartupApplicationListener.class);

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private PizzaRepository pizzaRepository;

    @Autowired
    private DrinkRepository drinkRepository;

    @Autowired
    private TownPointRepository townPointRepository;

    @Autowired
    private TownLinkRepository townLinkRepository;

    @Value("${security.admin.username}")
    private String adminUsername;

    @Value("${security.admin.password}")
    private String adminPassword;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        loadRolesAndUsers();
        loadProducts();
        loadTown();
    }

    private void loadRolesAndUsers() {
        LOG.info("Create roles");
        Role clientRole = Role.builder().roleType(RoleType.ROLE_CLIENT).build();
        Role driverRole = Role.builder().roleType(RoleType.ROLE_DRIVER).build();
        Role adminRole = Role.builder().roleType(RoleType.ROLE_ADMIN).build();
        if (!roleRepository.existsByRoleType(RoleType.ROLE_CLIENT))
            roleRepository.save(clientRole);
        if (!roleRepository.existsByRoleType(RoleType.ROLE_DRIVER))
            roleRepository.save(driverRole);
        if (!roleRepository.existsByRoleType(RoleType.ROLE_ADMIN))
            adminRole = roleRepository.save(adminRole);
        else
            adminRole = roleRepository.findRoleByRoleType(RoleType.ROLE_ADMIN).orElse(new Role());
        LOG.info("Create admin user");
        if (!userRepository.existsByUsername(adminUsername)) {
            User user = new User();
            user.setUsername(adminUsername);
            user.setPassword(encoder.encode(adminPassword));
            user.setRole(adminRole);
            userRepository.save(user);
        }
    }

    private void loadProducts() {
        if (pizzaRepository.count() == 0) {
            LOG.debug("Pizza catalog is empty, load pizzas");
            ObjectMapper objectMapper = new ObjectMapper();
            File pizzaFile = new File("src/main/resources/data/pizzas.json");
            try {
                List<Pizza> pizzas = objectMapper.readValue(pizzaFile, new TypeReference<List<Pizza>>() {});
                LOG.info("Pizzas were loaded, count={}", pizzas.size());
                pizzaRepository.saveAll(pizzas);
            } catch (IOException e) {
                LOG.error("Can't load pizzas");
                e.printStackTrace();
            }
        } else {
            LOG.info("Pizza catalog is not empty, skip loading pizzas");
        }
        if (drinkRepository.count() == 0) {
            LOG.debug("Drinks catalog is empty, load drinks");
            ObjectMapper objectMapper = new ObjectMapper();
            File pizzaFile = new File("src/main/resources/data/drinks.json");
            try {
                List<Drink> drinks = objectMapper.readValue(pizzaFile, new TypeReference<List<Drink>>() {});
                LOG.info("Pizzas were loaded, count={}", drinks.size());
                drinkRepository.saveAll(drinks);
            } catch (IOException e) {
                LOG.error("Can't load drinks");
                e.printStackTrace();
            }
        } else {
            LOG.info("Drinks catalog is not empty, skip loading drinks");
        }
    }

    private void loadTown() {
        if (townPointRepository.count() == 0) {
            LOG.info("Load town");
            String[] townStreetsY = new String[]{
                    "Gagarina",
                    "Minina",
                    "Pokrovskaya",
                    "Osharskaya",
                    "Lyadova",
                    "Zhukova"
            };
            String[] townStreetsX = new String[]{
                    "Yaroshenko",
                    "Moskovskaya",
                    "Komsomolskaya",
                    "Lenina",
                    "Belinskogo",
                    "Beketova"
            };
            TownPoint[] previousPoints = new TownPoint[11];
            int[] buildingX = new int[]{1, 1, 1, 1, 1, 1};
            int[] buildingY = new int[]{1, 1, 1, 1, 1, 1};
            for (int y = 0; y <= 10; ++y) {
                for (int x = 0; x <= 10; ++x) {
                    String street = ((x + y) % 4 == 0) ? townStreetsX[x / 2] : townStreetsY[y / 2];
                    String building = ((x + y) % 4 == 0) ? String.valueOf(buildingX[x / 2]++) : String.valueOf(buildingY[y / 2]++);
                    TownPoint newPoint = TownPoint.builder()
                            .street(street)
                            .building(building)
                            .build();
                    newPoint = townPointRepository.save(newPoint);
                    if (y != 0 && x % 2 == 0) {
                        TownLink townLink1 = TownLink.builder()
                                .sourceTownPoint(previousPoints[x])
                                .targetTownPoint(newPoint)
                                .duration(1)
                                .build();
                        TownLink townLink2 = TownLink.builder()
                                .sourceTownPoint(newPoint)
                                .targetTownPoint(previousPoints[x])
                                .duration(1)
                                .build();
                        townLinkRepository.save(townLink1);
                        townLinkRepository.save(townLink2);
                    }
                    if (x != 0) {
                        TownLink townLink1 = TownLink.builder()
                                .sourceTownPoint(previousPoints[x - 1])
                                .targetTownPoint(newPoint)
                                .duration(1)
                                .build();
                        TownLink townLink2 = TownLink.builder()
                                .sourceTownPoint(newPoint)
                                .targetTownPoint(previousPoints[x - 1])
                                .duration(1)
                                .build();
                        townLinkRepository.save(townLink1);
                        townLinkRepository.save(townLink2);
                    }
                    previousPoints[x] = newPoint;
                }
                if ((y++) == 10)
                    break;
                for (int x = 0; x < 6; ++x) {
                    String street = townStreetsX[x];
                    String building = String.valueOf(buildingX[x]++);
                    TownPoint newPoint = TownPoint.builder()
                            .street(street)
                            .building(building)
                            .build();
                    newPoint = townPointRepository.save(newPoint);
                    TownLink townLink1 = TownLink.builder()
                            .sourceTownPoint(previousPoints[x * 2])
                            .targetTownPoint(newPoint)
                            .duration(1)
                            .build();
                    TownLink townLink2 = TownLink.builder()
                            .sourceTownPoint(newPoint)
                            .targetTownPoint(previousPoints[x * 2])
                            .duration(1)
                            .build();
                    townLinkRepository.save(townLink1);
                    townLinkRepository.save(townLink2);
                    previousPoints[x * 2] = newPoint;
                }
            }
            LOG.info("Set branch offices");
            TownPoint townPoint = townPointRepository.findByStreetAndBuilding("Gagarina", "3").orElseThrow();
            townPoint.setIsBranchOffice(true);
            townPointRepository.save(townPoint);
            townPoint = townPointRepository.findByStreetAndBuilding("Belinskogo", "5").orElseThrow();
            townPoint.setIsBranchOffice(true);
            townPointRepository.save(townPoint);
            townPoint = townPointRepository.findByStreetAndBuilding("Lyadova", "2").orElseThrow();
            townPoint.setIsBranchOffice(true);
            townPointRepository.save(townPoint);
        } else {
            LOG.info("Town is not empty, skip loading town");
        }
    }

}
