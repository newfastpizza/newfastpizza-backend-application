package com.new_fast_pizza.component.security;

import com.new_fast_pizza.model.entity.User;
import com.new_fast_pizza.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthTokenFilter extends OncePerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(AuthTokenFilter.class);

    @Autowired
    private JwtComponent jwtComponent;

    @Autowired
    private UserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        LOG.debug("Do filter internal");
        try {
            String headerAuth = request.getHeader("Authorization");
            String jwt = (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) ?
                    headerAuth.substring(7) : null;
            if (jwt != null && jwtComponent.validateJwtToken(jwt)) {
                String username = jwtComponent.getUserNameFromJwtToken(jwt);
                LOG.debug("Defined username: {}", username);
                User user = userService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        user, null, user.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            } else {
                LOG.debug("Jwt token not set or invalid");
            }
        } catch (Exception e) {
            LOG.error("Cannot set user authentication:", e);
        }
        filterChain.doFilter(request, response);
    }
}
