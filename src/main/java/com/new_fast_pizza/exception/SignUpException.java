package com.new_fast_pizza.exception;

import org.springframework.security.core.AuthenticationException;

public class SignUpException extends AuthenticationException {

    public SignUpException(String message) {
        super(message);
    }
}
