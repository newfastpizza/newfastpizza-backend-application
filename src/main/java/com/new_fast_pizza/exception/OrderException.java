package com.new_fast_pizza.exception;

public class OrderException extends RuntimeException {

    public OrderException(String message) {
        super(message);
    }
}
