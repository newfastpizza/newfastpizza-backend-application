package com.new_fast_pizza.model.enums;

public enum RoleType {
    ROLE_CLIENT,
    ROLE_DRIVER,
    ROLE_ADMIN
}
