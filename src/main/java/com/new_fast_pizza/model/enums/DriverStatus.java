package com.new_fast_pizza.model.enums;

public enum DriverStatus {
    HAS_ORDER,
    FREE,
    OUT_OF_OFFICE
}
