package com.new_fast_pizza.model.enums;

public enum OrderStatus {
    IS_PREPARED,
    IS_DELIVERED,
    IS_FINISHED
}
