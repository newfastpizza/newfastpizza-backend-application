package com.new_fast_pizza.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@SuperBuilder
public class ChangeClientProfileRequest {

    @NotEmpty(message = "Name can't be empty")
    @Pattern(regexp = "[A-Za-z \\-]+", message = "Name can consist of uppercase and lowercase latin letters or spaces")
    private String name;

    @Email(message = "Email should have email format")
    private String email;

    @NotEmpty(message = "Phone can't be empty")
    @Pattern(regexp = "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$", message = "Phone should have phone format")
    private String phone;

}
