package com.new_fast_pizza.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@SuperBuilder
public class ChangePasswordRequest {

    private String oldPassword;

    @NotEmpty(message = "Password can't be empty")
    @Size(min = 5, message = "Password should be minimum 5 symbols")
    @Pattern(regexp = "^[0-9a-zA-Z!@#$%^&*()\\-_+={}\\[\\];:'\"|?/.,]+", message = "Password can consist of uppercase and lowercase latin letters or spec symbols")
    private String password;

    private String confirmPassword;

}
