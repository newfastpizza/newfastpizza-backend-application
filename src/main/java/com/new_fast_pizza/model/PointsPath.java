package com.new_fast_pizza.model;

import com.new_fast_pizza.model.entity.TownPoint;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class PointsPath {
    private List<TownPoint> path;
    private List<Integer> distances;
}
