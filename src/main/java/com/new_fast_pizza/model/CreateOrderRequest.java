package com.new_fast_pizza.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Map;

@Data
@NoArgsConstructor
@SuperBuilder
public class CreateOrderRequest {

    @NotEmpty(message = "Products list can't be empty")
    private Map<String, Integer> productsIds;

    @NotEmpty(message = "Address can't be empty")
    private String townPointId;

    private Integer flatNumber;

    private String comment;

    @NotEmpty(message = "Phone can't be empty")
    @Pattern(regexp = "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$", message = "Phone should have phone format")
    private String phone;

}
