package com.new_fast_pizza.model.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "town_points")
@SuperBuilder
public class TownPoint {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column
    private String street;

    @Column
    private String building;

    @Column(name = "is_branch_office")
    @Builder.Default
    private Boolean isBranchOffice = false;
}
