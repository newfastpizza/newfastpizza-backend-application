package com.new_fast_pizza.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.new_fast_pizza.model.enums.DriverStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@ToString(callSuper = true)
@Entity
@Table(name = "drivers")
@SuperBuilder
public class Driver extends User {

    @Column
    private String name;

    @Column
    private String phone;

    @ManyToOne
    @JoinColumn(name = "town_point_id")
    @JsonIgnore
    private TownPoint curAddress;

    @Column
    @Enumerated(EnumType.STRING)
    private DriverStatus status;

    @OneToOne
    @JoinColumn(name = "order_id")
    @JsonIgnore
    @ToString.Exclude
    private Order order;

}
