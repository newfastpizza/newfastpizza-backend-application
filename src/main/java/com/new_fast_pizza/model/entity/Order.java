package com.new_fast_pizza.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.new_fast_pizza.model.enums.OrderStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "orders")
@SuperBuilder
public class Order {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @Fetch(value = FetchMode.SELECT)
    private List<BacketItem> backet = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "town_point_id")
    private TownPoint townPoint;

    @Column(name = "flat_number")
    private Integer flatNumber;

    @Column
    private String phone;

    @Column
    private Date date;

    @Column
    private String comment;

    @Column
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @ManyToOne
    @JoinColumn(name = "office_id")
    private TownPoint office;

    @ManyToOne
    @JoinColumn(name = "driver_id")
    @JsonIgnore
    private Driver driver;

    @ManyToOne
    @JoinColumn(name = "client_id")
    @JsonIgnore
    private Client client;
}
