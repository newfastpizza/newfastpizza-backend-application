package com.new_fast_pizza.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "products")
@DiscriminatorColumn(name = "product_type", discriminatorType = DiscriminatorType.STRING)
@SuperBuilder
public abstract class Product {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column
    @NotEmpty(message = "Name can't be empty")
    private String name;

    @Column
    @NotEmpty(message = "Description can't be empty")
    private String description;

    @Lob
    @Column
    @NotEmpty(message = "Image can't be empty")
    private String image;

    @Column
    @NotNull(message = "Price can't be null")
    @Min(value = 1, message = "Price should be positive value")
    private Integer price;

    @Column
    @JsonIgnore
    @Builder.Default
    private Boolean isEnabled = true;
}
