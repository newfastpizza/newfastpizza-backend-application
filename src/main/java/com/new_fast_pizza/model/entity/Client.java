package com.new_fast_pizza.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@ToString(callSuper = true)
@Entity
@Table(name = "clients")
@SuperBuilder
public class Client extends User {

    @Column
    private String name;

    @Column
    private String phone;

    @Column
    private String email;

    @Column
    private Date birthday;

}
