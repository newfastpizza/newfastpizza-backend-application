package com.new_fast_pizza.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.new_fast_pizza.model.enums.RoleType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Data
@Entity
@Table(name = "roles")
@NoArgsConstructor
@SuperBuilder
public class Role implements GrantedAuthority {

    @JsonIgnore
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Enumerated(EnumType.STRING)
    @Column
    private RoleType roleType;

    @JsonIgnore
    @Override
    public String getAuthority() {
        return roleType.name();
    }
}
