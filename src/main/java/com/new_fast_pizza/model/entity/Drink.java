package com.new_fast_pizza.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
@SuperBuilder
public class Drink extends Product {

    @Column
    @NotNull(message = "Volume can't be null")
    @Min(value = 0, message = "Volume should be positive value")
    private Double volume;
}
