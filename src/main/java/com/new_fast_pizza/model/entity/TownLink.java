package com.new_fast_pizza.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "town_links")
@SuperBuilder
public class TownLink {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "source_id")
    private TownPoint sourceTownPoint;

    @ManyToOne
    @JoinColumn(name = "target_id")
    private TownPoint targetTownPoint;

    @Column
    private Integer duration;
}
