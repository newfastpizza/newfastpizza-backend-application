package com.new_fast_pizza.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@SuperBuilder
public class SignUpDriverRequest {

    @NotEmpty(message = "Username can't be empty")
    @Size(min = 3, message = "Username should be minimum 3 symbols")
    @Pattern(regexp = "[0-9a-zA-Z_]+", message = "Username can consist of uppercase and lowercase latin letters, numbers or underscores")
    private String username;

    @NotEmpty(message = "Name can't be empty")
    @Pattern(regexp = "[A-Za-z \\-]+", message = "Name can consist of uppercase and lowercase latin letters, spaces or dashes")
    private String name;

    @NotEmpty(message = "Phone can't be empty")
    @Pattern(regexp = "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$", message = "Phone should have phone format")
    private String phone;

    @NotEmpty(message = "Password can't be empty")
    @Size(min = 5, message = "Password should be minimum 5 symbols")
    @Pattern(regexp = "^[0-9a-zA-Z!@#$%^&*()\\-_+={}\\[\\];:'\"|?/.,]+", message = "Password can consist of uppercase and lowercase latin letters or spec symbols")
    private String password;

    private String confirmPassword;

}
