package com.new_fast_pizza.repository;

import com.new_fast_pizza.model.entity.Driver;
import com.new_fast_pizza.model.enums.DriverStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DriverRepository extends JpaRepository<Driver, String> {
    List<Driver> findByIsEnabledTrue();

    List<Driver> findByStatus(DriverStatus driverStatus);

    Optional<Driver> findByIdAndIsEnabledTrue(String id);
}
