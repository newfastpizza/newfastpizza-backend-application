package com.new_fast_pizza.repository;

import com.new_fast_pizza.model.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, String> {
    List<Client> findByIsEnabledTrue();

    Optional<Client> findByIdAndIsEnabledTrue(String id);
}
