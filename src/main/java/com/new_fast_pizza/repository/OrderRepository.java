package com.new_fast_pizza.repository;

import com.new_fast_pizza.model.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, String> {
    List<Order> findByClient_id(String id);

    List<Order> findByDriver_id(String driverId);
}
