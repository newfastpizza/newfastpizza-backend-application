package com.new_fast_pizza.repository;

import com.new_fast_pizza.model.entity.Drink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DrinkRepository extends JpaRepository<Drink, String> {
    List<Drink> findByIsEnabledTrue();

    Optional<Drink> findByIdAndIsEnabledTrue(String id);
}
