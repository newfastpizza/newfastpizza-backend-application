package com.new_fast_pizza.repository;

import com.new_fast_pizza.model.entity.Pizza;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PizzaRepository extends JpaRepository<Pizza, String> {
    List<Pizza> findByIsEnabledTrue();

    Optional<Pizza> findByIdAndIsEnabledTrue(String id);
}
