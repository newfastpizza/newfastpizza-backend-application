package com.new_fast_pizza.repository;

import com.new_fast_pizza.model.entity.TownLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TownLinkRepository extends JpaRepository<TownLink, String> {
}
