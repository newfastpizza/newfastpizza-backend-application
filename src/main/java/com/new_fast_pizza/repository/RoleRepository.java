package com.new_fast_pizza.repository;

import com.new_fast_pizza.model.entity.Role;
import com.new_fast_pizza.model.enums.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
    Optional<Role> findRoleByRoleType(RoleType roleType);

    Boolean existsByRoleType(RoleType roleType);
}
