package com.new_fast_pizza.repository;

import com.new_fast_pizza.model.entity.TownPoint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TownPointRepository extends JpaRepository<TownPoint, String> {
    List<TownPoint> findAllByIsBranchOfficeTrue();

    Optional<TownPoint> findByStreetAndBuilding(String street, String building);
}
