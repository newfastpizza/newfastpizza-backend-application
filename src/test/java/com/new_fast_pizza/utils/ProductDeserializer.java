package com.new_fast_pizza.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.new_fast_pizza.model.entity.Drink;
import com.new_fast_pizza.model.entity.Pizza;
import com.new_fast_pizza.model.entity.Product;

import java.io.IOException;

public class ProductDeserializer extends StdDeserializer<Product> {

    public ProductDeserializer() {
        this(null);
    }

    public ProductDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Product deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);

        String id = node.get("id").asText(null);
        String name = node.get("name").asText(null);
        String description = node.get("description").asText(null);
        String image = node.get("image").asText(null);
        Integer price = node.has("price") ? node.get("price").asInt() : null;
        String size = node.get("price").asText(null);
        Double volume = node.has("volume") ? node.get("volume").asDouble() : null;

        if (volume != null)
            return Drink.builder()
                    .id(id)
                    .name(name)
                    .description(description)
                    .image(image)
                    .price(price)
                    .volume(volume)
                    .build();
        else
            return Pizza.builder()
                    .id(id)
                    .name(name)
                    .description(description)
                    .image(image)
                    .price(price)
                    .size(size)
                    .build();
    }
}
