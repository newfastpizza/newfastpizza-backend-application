package com.new_fast_pizza.controller;

import com.new_fast_pizza.Application;
import com.new_fast_pizza.component.AuthOperations;
import com.new_fast_pizza.model.ChangeClientProfileRequest;
import com.new_fast_pizza.model.entity.Client;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ClientControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private AuthOperations authOperations;

    @Value("${security.admin.username}")
    private String adminUsername;

    @Value("${security.admin.password}")
    private String adminPassword;

    @BeforeAll
    public void addUsers() {
        try {
            authOperations.signUpClient("test_client", "test_client");
            authOperations.signUpDriver("test_driver", "test_driver");
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getClientProfile() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Client> profileEntity = testRestTemplate.exchange("/api/clients/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Client.class);
        Assertions.assertEquals(200, profileEntity.getStatusCode().value());
        Client client = profileEntity.getBody();
        Assertions.assertNotNull(client);
        Assertions.assertEquals("test_client", client.getUsername());
        Assertions.assertNull(client.getPassword());
    }

    @Test
    public void getClientProfileWithoutAuthorization() {
        ResponseEntity<String> profileEntity = testRestTemplate.getForEntity("/api/clients/profile", String.class);
        Assertions.assertEquals(401, profileEntity.getStatusCode().value());
    }

    @Test
    public void getClientProfileAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> profileEntity = testRestTemplate.exchange("/api/clients/profile", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(403, profileEntity.getStatusCode().value());
    }

    @Test
    public void getClientProfileAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> profileEntity = testRestTemplate.exchange("/api/clients/profile", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(403, profileEntity.getStatusCode().value());
    }

    @Test
    public void getAllClients() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Client[]> clientsEntity = testRestTemplate.exchange("/api/clients", HttpMethod.GET,
                new HttpEntity<>(headers), Client[].class);
        Assertions.assertEquals(200, clientsEntity.getStatusCode().value());
        Client[] clientsArray = clientsEntity.getBody();
        Assertions.assertNotNull(clientsArray);
        List<Client> clients = Arrays.asList(clientsArray);
        Assertions.assertNotEquals(0, clients.size());
        Assertions.assertTrue(clients.stream().anyMatch(client -> client.getUsername().equals("test_client")));
    }

    @Test
    public void getAllClientsWithoutAuthorization() {
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<String> clientsEntity = testRestTemplate.getForEntity("/api/clients", String.class);
        Assertions.assertEquals(401, clientsEntity.getStatusCode().value());
    }

    @Test
    public void getAllClientAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> clientsEntity = testRestTemplate.exchange("/api/clients", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(403, clientsEntity.getStatusCode().value());
    }

    @Test
    public void getAllClientsAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> clientsEntity = testRestTemplate.exchange("/api/clients", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(403, clientsEntity.getStatusCode().value());
    }

    @Test
    public void getClientByIdAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Client> profileEntity = testRestTemplate.exchange("/api/clients/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Client.class);
        Client client = profileEntity.getBody();
        Assertions.assertNotNull(client);
        Assertions.assertNotNull(client.getId());

        ResponseEntity<Client> clientByIdEntity = testRestTemplate.exchange("/api/clients/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), Client.class, client.getId());
        Assertions.assertEquals(200, clientByIdEntity.getStatusCode().value());
        Client clientById = clientByIdEntity.getBody();
        Assertions.assertEquals(client, clientById);

        clientByIdEntity = testRestTemplate.exchange("/api/clients/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), Client.class, "not-exist-id");
        Assertions.assertEquals(403, clientByIdEntity.getStatusCode().value());
    }

    @Test
    public void getClientByIdAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Client[]> clientsEntity = testRestTemplate.exchange("/api/clients", HttpMethod.GET,
                new HttpEntity<>(headers), Client[].class);
        Client[] clientsArray = clientsEntity.getBody();
        Assertions.assertNotNull(clientsArray);
        List<Client> clients = Arrays.asList(clientsArray);
        Client client = clients.stream().filter(client1 -> client1.getUsername().equals("test_client")).findAny().orElseThrow();

        ResponseEntity<Client> clientByIdEntity = testRestTemplate.exchange("/api/clients/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), Client.class, client.getId());
        Assertions.assertEquals(200, clientByIdEntity.getStatusCode().value());
        Client clientById = clientByIdEntity.getBody();
        Assertions.assertEquals(client, clientById);

        clientByIdEntity = testRestTemplate.exchange("/api/clients/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), Client.class, "not-exist-id");
        Assertions.assertEquals(404, clientByIdEntity.getStatusCode().value());
    }

    @Test
    public void getClientByIdAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Client> clientByIdEntity = testRestTemplate.exchange("/api/clients/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), Client.class, "some-id");
        Assertions.assertEquals(403, clientByIdEntity.getStatusCode().value());
    }

    @Test
    public void getClientByIdWithoutAuthorization() {
        ResponseEntity<Client> clientByIdEntity = testRestTemplate.getForEntity("/api/clients/{id}",
                Client.class, "some-id");
        Assertions.assertEquals(401, clientByIdEntity.getStatusCode().value());
    }

    @Test
    public void changeClientProfile() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Client> profileEntity = testRestTemplate.exchange("/api/clients/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Client.class);
        Assertions.assertEquals(200, profileEntity.getStatusCode().value());
        Client client = profileEntity.getBody();
        Assertions.assertNotNull(client);

        ChangeClientProfileRequest changeClientProfileRequest = ChangeClientProfileRequest.builder()
                .name("Test Client")
                .email(client.getEmail())
                .phone(client.getPhone())
                .build();
        ResponseEntity<Client> changeProfileEntity = testRestTemplate.postForEntity("/api/clients/profile",
                new HttpEntity<>(changeClientProfileRequest, headers), Client.class);
        Assertions.assertEquals(200, changeProfileEntity.getStatusCode().value());
        Client newProfile = changeProfileEntity.getBody();
        Assertions.assertNotNull(newProfile);
        Assertions.assertNotEquals(client, newProfile);
        client.setName(newProfile.getName());
        Assertions.assertEquals(client, newProfile);

        profileEntity = testRestTemplate.exchange("/api/clients/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Client.class);
        Assertions.assertEquals(200, profileEntity.getStatusCode().value());
        client = profileEntity.getBody();
        Assertions.assertNotNull(client);
        Assertions.assertEquals(newProfile, client);
    }

    @Test
    public void changeClientProfileAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ChangeClientProfileRequest changeClientProfileRequest = ChangeClientProfileRequest.builder()
                .name("Test Client")
                .email("someEmail@gmail.com")
                .phone("8-910-224-56-45")
                .build();
        ResponseEntity<Client> changeProfileEntity = testRestTemplate.postForEntity("/api/clients/profile",
                new HttpEntity<>(changeClientProfileRequest, headers), Client.class);
        Assertions.assertEquals(403, changeProfileEntity.getStatusCode().value());
    }

    @Test
    public void changeClientProfileAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ChangeClientProfileRequest changeClientProfileRequest = ChangeClientProfileRequest.builder()
                .name("Test Client")
                .email("someEmail@gmail.com")
                .phone("8-910-224-56-45")
                .build();
        ResponseEntity<Client> changeProfileEntity = testRestTemplate.postForEntity("/api/clients/profile",
                new HttpEntity<>(changeClientProfileRequest, headers), Client.class);
        Assertions.assertEquals(403, changeProfileEntity.getStatusCode().value());
    }

    @Test
    public void changeClientProfileWithoutAuthorization() {
        ChangeClientProfileRequest changeClientProfileRequest = ChangeClientProfileRequest.builder()
                .name("Test Client")
                .email("someEmail@gmail.com")
                .phone("8-910-224-56-45")
                .build();
        ResponseEntity<Client> changeProfileEntity = testRestTemplate.postForEntity("/api/clients/profile",
                changeClientProfileRequest, Client.class);
        Assertions.assertEquals(401, changeProfileEntity.getStatusCode().value());
    }

    @Test
    public void checkChangeClientProfileValidation() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        BiConsumer<ChangeClientProfileRequest, String> tryChange = (request, message) -> {
            ResponseEntity<String> changeProfileEntity = testRestTemplate.postForEntity("/api/clients/profile",
                    new HttpEntity<>(request, headers), String.class);
            Assertions.assertEquals(400, changeProfileEntity.getStatusCode().value());
            Assertions.assertEquals(message, changeProfileEntity.getBody());
        };

        ChangeClientProfileRequest changeClientProfileRequest = ChangeClientProfileRequest.builder()
                .name("Test Client")
                .email("client@gmail.com")
                .phone("8-831-554-66-32")
                .build();

        changeClientProfileRequest.setName(null);
        tryChange.accept(changeClientProfileRequest, "Name can't be empty");
        changeClientProfileRequest.setName("Test Client1");
        tryChange.accept(changeClientProfileRequest, "Name can consist of uppercase and lowercase latin letters or spaces");
        changeClientProfileRequest.setName("Test Name");

        changeClientProfileRequest.setEmail("some-email");
        tryChange.accept(changeClientProfileRequest, "Email should have email format");
        changeClientProfileRequest.setEmail("client@gmail.com");

        changeClientProfileRequest.setPhone(null);
        tryChange.accept(changeClientProfileRequest, "Phone can't be empty");
        changeClientProfileRequest.setPhone("some-phone");
        tryChange.accept(changeClientProfileRequest, "Phone should have phone format");
    }

    @Test
    public void deleteClient() {
        authOperations.signUpClient("deleted_client", "deleted_client");
        String jwtToken = authOperations.login("deleted_client", "deleted_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Client> deleteProfileEntity = testRestTemplate.exchange("/api/clients/profile", HttpMethod.DELETE,
                new HttpEntity<>(headers), Client.class);
        Assertions.assertEquals(200, deleteProfileEntity.getStatusCode().value());
        Client client = deleteProfileEntity.getBody();
        Assertions.assertNotNull(client);
        Assertions.assertEquals("deleted_client", client.getUsername());

        jwtToken = authOperations.login(adminUsername, adminPassword);
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);
        ResponseEntity<Client[]> getClientsEntity = testRestTemplate.exchange("/api/clients", HttpMethod.GET,
                new HttpEntity<>(headers), Client[].class);
        Assertions.assertEquals(200, getClientsEntity.getStatusCode().value());
        Client[] clientsArray = getClientsEntity.getBody();
        Assertions.assertNotNull(clientsArray);
        List<Client> clients = Arrays.asList(clientsArray);
        Assertions.assertFalse(clients.stream().anyMatch(client1 -> client1.getUsername().equals("deleted_client")));
    }

    @Test
    public void deleteClientAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Client> deleteProfileEntity = testRestTemplate.exchange("/api/clients/profile", HttpMethod.DELETE,
                new HttpEntity<>(headers), Client.class);
        Assertions.assertEquals(403, deleteProfileEntity.getStatusCode().value());
    }

    @Test
    public void deleteClientAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Client> deleteProfileEntity = testRestTemplate.exchange("/api/clients/profile", HttpMethod.DELETE,
                new HttpEntity<>(headers), Client.class);
        Assertions.assertEquals(403, deleteProfileEntity.getStatusCode().value());
    }

    @Test
    public void deleteClientWithoutAuthorization() {
        ResponseEntity<Client> deleteProfileEntity = testRestTemplate.getForEntity("/api/clients/profile", Client.class);
        Assertions.assertEquals(401, deleteProfileEntity.getStatusCode().value());
    }

}
