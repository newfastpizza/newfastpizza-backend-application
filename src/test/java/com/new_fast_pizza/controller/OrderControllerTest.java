package com.new_fast_pizza.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.new_fast_pizza.Application;
import com.new_fast_pizza.component.AuthOperations;
import com.new_fast_pizza.component.DriverOperations;
import com.new_fast_pizza.component.ProductOperations;
import com.new_fast_pizza.component.TownOperations;
import com.new_fast_pizza.model.CreateOrderRequest;
import com.new_fast_pizza.model.entity.*;
import com.new_fast_pizza.model.enums.DriverStatus;
import com.new_fast_pizza.model.enums.OrderStatus;
import com.new_fast_pizza.utils.ProductDeserializer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@EnableAsync
public class OrderControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private AuthOperations authOperations;

    @Autowired
    private TownOperations townOperations;

    @Autowired
    private ProductOperations productOperations;

    @Autowired
    private DriverOperations driverOperations;

    @Value("${security.admin.username}")
    private String adminUsername;

    @Value("${security.admin.password}")
    private String adminPassword;

    @BeforeAll
    public void addUsers() {
        try {
            authOperations.signUpClient("test_client", "test_client");
            authOperations.signUpDriver("test_driver", "test_driver");
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getAllOrdersAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> ordersEntity = testRestTemplate.exchange("/api/orders", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(200, ordersEntity.getStatusCode().value());
        String ordersArray = ordersEntity.getBody();
        Assertions.assertNotNull(ordersArray);
    }

    @Test
    public void getAllOrdersAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> ordersEntity = testRestTemplate.exchange("/api/orders", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(200, ordersEntity.getStatusCode().value());
        String ordersArray = ordersEntity.getBody();
        Assertions.assertNotNull(ordersArray);
    }

    @Test
    public void getAllOrdersAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> ordersEntity = testRestTemplate.exchange("/api/orders", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(200, ordersEntity.getStatusCode().value());
        String ordersArray = ordersEntity.getBody();
        Assertions.assertNotNull(ordersArray);
    }

    @Test
    public void getAllOrdersWithoutAuthorization() {
        ResponseEntity<String> ordersEntity = testRestTemplate.getForEntity("/api/orders", String.class);
        Assertions.assertEquals(401, ordersEntity.getStatusCode().value());
    }

    @Test
    public void checkOrderWithClientIdLifeCircle() throws JsonProcessingException, InterruptedException {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        String adminToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders adminHeaders = new HttpHeaders();
        adminHeaders.add("Authorization", "Bearer " + adminToken);

        String driverToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders driverHeaders = new HttpHeaders();
        driverHeaders.add("Authorization", "Bearer " + driverToken);

        List<Pizza> pizzas = productOperations.getPizzas();
        List<Drink> drinks = productOperations.getDrinks();
        Map<String, Integer> productIds = new HashMap<>() {{
            put(pizzas.get(0).getId(), 1);
            put(pizzas.get(1).getId(), 1);
            put(drinks.get(0).getId(), 3);
        }};
        TownPoint townPoint = townOperations.getTownByAddress("Gagarina", "1");
        CreateOrderRequest createOrderRequest = CreateOrderRequest.builder()
                .productsIds(productIds)
                .townPointId(townPoint.getId())
                .flatNumber(16)
                .comment("some comment")
                .phone("8-910-245-66-35")
                .build();

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Product.class, new ProductDeserializer());
        mapper.registerModule(module);

        ResponseEntity<String> orderEntity = testRestTemplate.postForEntity("/api/orders",
                new HttpEntity<>(createOrderRequest, headers), String.class);
        Assertions.assertEquals(200, orderEntity.getStatusCode().value());
        String orderJson = orderEntity.getBody();
        Assertions.assertNotNull(orderJson);
        Order order = mapper.readValue(orderJson, Order.class);
        Assertions.assertEquals(3, order.getBacket().size());
        Assertions.assertEquals(townPoint, order.getTownPoint());
        Assertions.assertTrue(order.getOffice().getIsBranchOffice());
        Assertions.assertEquals(OrderStatus.IS_PREPARED, order.getStatus());

        ResponseEntity<String> ordersEntity = testRestTemplate.exchange("/api/orders", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(200, ordersEntity.getStatusCode().value());
        orderJson = ordersEntity.getBody();
        Assertions.assertNotNull(orderJson);
        Order[] ordersArray = mapper.readValue(orderJson, Order[].class);
        List<Order> orders = Arrays.asList(ordersArray);
        Assertions.assertEquals(1, orders.size());

        ResponseEntity<Client> clientEntity = testRestTemplate.exchange("/api/orders/{id}/client", HttpMethod.GET,
                new HttpEntity<>(adminHeaders), Client.class, order.getId());
        Assertions.assertEquals(200, clientEntity.getStatusCode().value());
        Client client = clientEntity.getBody();
        Assertions.assertNotNull(client);
        Assertions.assertEquals("test_client", client.getUsername());

        ResponseEntity<Driver> driverEntity = testRestTemplate.exchange("/api/orders/{id}/driver", HttpMethod.GET,
                new HttpEntity<>(adminHeaders), Driver.class, order.getId());
        Assertions.assertEquals(200, clientEntity.getStatusCode().value());
        Driver driver = driverEntity.getBody();
        Assertions.assertNull(driver);

        Thread.sleep(30000);
        townPoint = townOperations.getTownByAddress("Gagarina", "2");
        driverOperations.setPosition("test_driver", townPoint);
        driverOperations.setStatus("test_driver", DriverStatus.FREE);
        Thread.sleep(60000);

        driverEntity = testRestTemplate.exchange("/api/orders/{id}/driver", HttpMethod.GET,
                new HttpEntity<>(adminHeaders), Driver.class, order.getId());
        Assertions.assertEquals(200, clientEntity.getStatusCode().value());
        driver = driverEntity.getBody();
        Assertions.assertNotNull(driver);
        Assertions.assertEquals(DriverStatus.HAS_ORDER, driver.getStatus());

        orderEntity = testRestTemplate.exchange("/api/orders/{id}/deliver", HttpMethod.GET,
                new HttpEntity<>(driverHeaders), String.class, order.getId());
        Assertions.assertEquals(200, orderEntity.getStatusCode().value());
        orderJson = orderEntity.getBody();
        Assertions.assertNotNull(orderJson);
        order = mapper.readValue(orderJson, Order.class);
        Assertions.assertEquals(OrderStatus.IS_DELIVERED, order.getStatus());

        orderEntity = testRestTemplate.exchange("/api/orders/{id}/finish", HttpMethod.GET,
                new HttpEntity<>(driverHeaders), String.class, order.getId());
        Assertions.assertEquals(200, orderEntity.getStatusCode().value());
        orderJson = orderEntity.getBody();
        Assertions.assertNotNull(orderJson);
        order = mapper.readValue(orderJson, Order.class);
        Assertions.assertEquals(OrderStatus.IS_FINISHED, order.getStatus());

        driverEntity = testRestTemplate.exchange("/api/orders/{id}/driver", HttpMethod.GET,
                new HttpEntity<>(adminHeaders), Driver.class, order.getId());
        Assertions.assertEquals(200, clientEntity.getStatusCode().value());
        driver = driverEntity.getBody();
        Assertions.assertNotNull(driver);
        Assertions.assertEquals(DriverStatus.FREE, driver.getStatus());

        driverOperations.setStatus("test_driver", DriverStatus.OUT_OF_OFFICE);

        ordersEntity = testRestTemplate.exchange("/api/orders", HttpMethod.GET,
                new HttpEntity<>(driverHeaders), String.class);
        Assertions.assertEquals(200, ordersEntity.getStatusCode().value());
        orderJson = ordersEntity.getBody();
        Assertions.assertNotNull(orderJson);
        ordersArray = mapper.readValue(orderJson, Order[].class);
        orders = Arrays.asList(ordersArray);
        Assertions.assertEquals(1, orders.size());
    }

    @Test
    public void checkOrderWithoutClientIdLifeCircle() throws JsonProcessingException {
        String adminToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders adminHeaders = new HttpHeaders();
        adminHeaders.add("Authorization", "Bearer " + adminToken);

        List<Pizza> pizzas = productOperations.getPizzas();
        List<Drink> drinks = productOperations.getDrinks();
        Map<String, Integer> productIds = new HashMap<>() {{
            put(pizzas.get(0).getId(), 1);
            put(pizzas.get(1).getId(), 1);
            put(drinks.get(0).getId(), 3);
        }};
        TownPoint townPoint = townOperations.getTownByAddress("Gagarina", "1");
        CreateOrderRequest createOrderRequest = CreateOrderRequest.builder()
                .productsIds(productIds)
                .townPointId(townPoint.getId())
                .flatNumber(16)
                .comment("some comment")
                .phone("8-910-245-66-35")
                .build();

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Product.class, new ProductDeserializer());
        mapper.registerModule(module);

        ResponseEntity<String> orderEntity = testRestTemplate.postForEntity("/api/orders",
                createOrderRequest, String.class);
        Assertions.assertEquals(200, orderEntity.getStatusCode().value());
        String orderJson = orderEntity.getBody();
        Assertions.assertNotNull(orderJson);
        Order order = mapper.readValue(orderJson, Order.class);
        Assertions.assertEquals(3, order.getBacket().size());
        Assertions.assertEquals(townPoint, order.getTownPoint());
        Assertions.assertTrue(order.getOffice().getIsBranchOffice());
        Assertions.assertEquals(OrderStatus.IS_PREPARED, order.getStatus());

        orderEntity = testRestTemplate.exchange("/api/orders/{id}", HttpMethod.DELETE,
                new HttpEntity<>(adminHeaders), String.class, order.getId());
        Assertions.assertEquals(200, orderEntity.getStatusCode().value());
        orderJson = orderEntity.getBody();
        Assertions.assertNotNull(orderJson);
        order = mapper.readValue(orderJson, Order.class);
        Assertions.assertNotNull(order);

        orderEntity = testRestTemplate.exchange("/api/orders/{id}", HttpMethod.GET,
                new HttpEntity<>(adminHeaders), String.class, order.getId());
        Assertions.assertEquals(404, orderEntity.getStatusCode().value());
    }

}
