package com.new_fast_pizza.controller;

import com.new_fast_pizza.Application;
import com.new_fast_pizza.component.AuthOperations;
import com.new_fast_pizza.model.entity.Drink;
import com.new_fast_pizza.model.entity.Pizza;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private AuthOperations authOperations;

    @Value("${security.admin.username}")
    private String adminUsername;

    @Value("${security.admin.password}")
    private String adminPassword;

    @BeforeAll
    public void addUsers() {
        try {
            authOperations.signUpClient("test_client", "test_client");
            authOperations.signUpDriver("test_driver", "test_driver");
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getAllProducts() {
        ResponseEntity<Object[]> productsEntity = testRestTemplate.getForEntity("/api/products", Object[].class);
        Assertions.assertEquals(200, productsEntity.getStatusCode().value());
        Object[] productArray = productsEntity.getBody();
        Assertions.assertNotNull(productArray);
        List<Object> products = Arrays.asList(productArray);
        Assertions.assertEquals(28, products.size());
    }

    @Test
    public void getAllPizzas() {
        ResponseEntity<Pizza[]> pizzasEntity = testRestTemplate.getForEntity("/api/products/pizzas", Pizza[].class);
        Assertions.assertEquals(200, pizzasEntity.getStatusCode().value());
        Pizza[] pizzasArray = pizzasEntity.getBody();
        Assertions.assertNotNull(pizzasArray);
        List<Pizza> pizzas = Arrays.asList(pizzasArray);
        Assertions.assertEquals(21, pizzas.size());
    }

    @Test
    public void updatePizzaAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        Pizza pizza = Pizza.builder()
                .name("New Pizza")
                .description("New pizza description")
                .image("some-image-code")
                .price(100)
                .size("40sm")
                .build();

        ResponseEntity<Pizza> pizzaEntity = testRestTemplate.postForEntity("/api/products/pizzas",
                new HttpEntity<>(pizza, headers), Pizza.class);
        Assertions.assertEquals(200, pizzaEntity.getStatusCode().value());
        Pizza newPizza = pizzaEntity.getBody();
        Assertions.assertNotNull(newPizza);
        pizza.setId(newPizza.getId());
        Assertions.assertEquals(pizza, newPizza);

        pizzaEntity = testRestTemplate.getForEntity("/api/products/{id}", Pizza.class, newPizza.getId());
        Assertions.assertEquals(200, pizzaEntity.getStatusCode().value());
        newPizza = pizzaEntity.getBody();
        Assertions.assertNotNull(newPizza);
        Assertions.assertEquals(pizza, newPizza);

        pizza.setDescription("New description");
        pizzaEntity = testRestTemplate.postForEntity("/api/products/pizzas",
                new HttpEntity<>(pizza, headers), Pizza.class);
        Assertions.assertEquals(200, pizzaEntity.getStatusCode().value());
        newPizza = pizzaEntity.getBody();
        Assertions.assertNotNull(newPizza);
        Assertions.assertEquals(pizza, newPizza);

        pizzaEntity = testRestTemplate.exchange("/api/products/{id}", HttpMethod.DELETE, new HttpEntity<>(headers),
                Pizza.class, newPizza.getId());
        Assertions.assertEquals(200, pizzaEntity.getStatusCode().value());
        newPizza = pizzaEntity.getBody();
        Assertions.assertNotNull(newPizza);
        Assertions.assertEquals(pizza, newPizza);

        pizzaEntity = testRestTemplate.getForEntity("/api/products/{id}", Pizza.class, newPizza.getId());
        Assertions.assertEquals(404, pizzaEntity.getStatusCode().value());
    }

    @Test
    public void updatePizzaAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        Pizza pizza = Pizza.builder()
                .name("New Pizza")
                .description("New pizza description")
                .image("some-image-code")
                .price(100)
                .size("40sm")
                .build();

        ResponseEntity<Pizza> pizzaEntity = testRestTemplate.postForEntity("/api/products/pizzas",
                new HttpEntity<>(pizza, headers), Pizza.class);
        Assertions.assertEquals(403, pizzaEntity.getStatusCode().value());
    }

    @Test
    public void updatePizzaAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        Pizza pizza = Pizza.builder()
                .name("New Pizza")
                .description("New pizza description")
                .image("some-image-code")
                .price(100)
                .size("40sm")
                .build();

        ResponseEntity<Pizza> pizzaEntity = testRestTemplate.postForEntity("/api/products/pizzas",
                new HttpEntity<>(pizza, headers), Pizza.class);
        Assertions.assertEquals(403, pizzaEntity.getStatusCode().value());
    }

    @Test
    public void updatePizzaWithoutAuthorization() {
        Pizza pizza = Pizza.builder()
                .name("New Pizza")
                .description("New pizza description")
                .image("some-image-code")
                .price(100)
                .size("40sm")
                .build();

        ResponseEntity<Pizza> pizzaEntity = testRestTemplate.postForEntity("/api/products/pizzas", pizza, Pizza.class);
        Assertions.assertEquals(401, pizzaEntity.getStatusCode().value());
    }

    @Test
    public void checkUpdatePizzaValidation() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        BiConsumer<Pizza, String> tryUpdate = (pizza, message) -> {
            ResponseEntity<String> pizzaEntity = testRestTemplate.postForEntity("/api/products/pizzas",
                    new HttpEntity<>(pizza, headers), String.class);
            Assertions.assertEquals(400, pizzaEntity.getStatusCode().value());
            Assertions.assertEquals(message, pizzaEntity.getBody());
        };

        Pizza pizza = Pizza.builder()
                .name("New Pizza")
                .description("New pizza description")
                .image("some-image-code")
                .price(100)
                .size("40sm")
                .build();

        pizza.setName(null);
        tryUpdate.accept(pizza, "Name can't be empty");
        pizza.setName("New Pizza");

        pizza.setDescription(null);
        tryUpdate.accept(pizza, "Description can't be empty");
        pizza.setDescription("New pizza description");

        pizza.setImage(null);
        tryUpdate.accept(pizza, "Image can't be empty");
        pizza.setImage("some-image-code");

        pizza.setPrice(null);
        tryUpdate.accept(pizza, "Price can't be null");
        pizza.setPrice(-1);
        tryUpdate.accept(pizza, "Price should be positive value");
        pizza.setPrice(100);

        pizza.setSize(null);
        tryUpdate.accept(pizza, "Size can't be empty");
    }

    @Test
    public void getAllDrinks() {
        ResponseEntity<Drink[]> drinksEntity = testRestTemplate.getForEntity("/api/products/drinks", Drink[].class);
        Assertions.assertEquals(200, drinksEntity.getStatusCode().value());
        Drink[] drinksArray = drinksEntity.getBody();
        Assertions.assertNotNull(drinksArray);
        List<Drink> drinks = Arrays.asList(drinksArray);
        Assertions.assertEquals(7, drinks.size());
    }

    @Test
    public void updateDrinkAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        Drink drink = Drink.builder()
                .name("New Drink")
                .description("New drink description")
                .image("some-image-code")
                .price(100)
                .volume(0.5)
                .build();

        ResponseEntity<Drink> drinkEntity = testRestTemplate.postForEntity("/api/products/drinks",
                new HttpEntity<>(drink, headers), Drink.class);
        Assertions.assertEquals(200, drinkEntity.getStatusCode().value());
        Drink newDrink = drinkEntity.getBody();
        Assertions.assertNotNull(newDrink);
        drink.setId(newDrink.getId());
        Assertions.assertEquals(drink, newDrink);

        drinkEntity = testRestTemplate.getForEntity("/api/products/{id}", Drink.class, newDrink.getId());
        Assertions.assertEquals(200, drinkEntity.getStatusCode().value());
        newDrink = drinkEntity.getBody();
        Assertions.assertNotNull(newDrink);
        Assertions.assertEquals(drink, newDrink);

        drink.setDescription("New description");
        drinkEntity = testRestTemplate.postForEntity("/api/products/drinks",
                new HttpEntity<>(drink, headers), Drink.class);
        Assertions.assertEquals(200, drinkEntity.getStatusCode().value());
        newDrink = drinkEntity.getBody();
        Assertions.assertNotNull(newDrink);
        Assertions.assertEquals(drink, newDrink);

        drinkEntity = testRestTemplate.exchange("/api/products/{id}", HttpMethod.DELETE, new HttpEntity<>(headers),
                Drink.class, newDrink.getId());
        Assertions.assertEquals(200, drinkEntity.getStatusCode().value());
        newDrink = drinkEntity.getBody();
        Assertions.assertNotNull(newDrink);
        Assertions.assertEquals(drink, newDrink);

        drinkEntity = testRestTemplate.getForEntity("/api/products/{id}", Drink.class, newDrink.getId());
        Assertions.assertEquals(404, drinkEntity.getStatusCode().value());
    }

    @Test
    public void updateDrinkAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        Drink drink = Drink.builder()
                .name("New Drink")
                .description("New drink description")
                .image("some-image-code")
                .price(100)
                .volume(0.5)
                .build();

        ResponseEntity<Drink> drinkEntity = testRestTemplate.postForEntity("/api/products/drinks",
                new HttpEntity<>(drink, headers), Drink.class);
        Assertions.assertEquals(403, drinkEntity.getStatusCode().value());
    }

    @Test
    public void updateDrinkAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        Drink drink = Drink.builder()
                .name("New Drink")
                .description("New drink description")
                .image("some-image-code")
                .price(100)
                .volume(0.5)
                .build();

        ResponseEntity<Drink> drinkEntity = testRestTemplate.postForEntity("/api/products/drinks",
                new HttpEntity<>(drink, headers), Drink.class);
        Assertions.assertEquals(403, drinkEntity.getStatusCode().value());
    }

    @Test
    public void updateDrinkWithoutAuthorization() {
        Drink drink = Drink.builder()
                .name("New Drink")
                .description("New drink description")
                .image("some-image-code")
                .price(100)
                .volume(0.5)
                .build();

        ResponseEntity<Drink> drinkEntity = testRestTemplate.postForEntity("/api/products/drinks", drink, Drink.class);
        Assertions.assertEquals(401, drinkEntity.getStatusCode().value());
    }

    @Test
    public void checkUpdateDrinkValidation() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        BiConsumer<Drink, String> tryUpdate = (drink, message) -> {
            ResponseEntity<String> drinkEntity = testRestTemplate.postForEntity("/api/products/drinks",
                    new HttpEntity<>(drink, headers), String.class);
            Assertions.assertEquals(400, drinkEntity.getStatusCode().value());
            Assertions.assertEquals(message, drinkEntity.getBody());
        };

        Drink drink = Drink.builder()
                .name("New Drink")
                .description("New drink description")
                .image("some-image-code")
                .price(100)
                .volume(0.5)
                .build();

        drink.setName(null);
        tryUpdate.accept(drink, "Name can't be empty");
        drink.setName("New Drink");

        drink.setDescription(null);
        tryUpdate.accept(drink, "Description can't be empty");
        drink.setDescription("New drink description");

        drink.setImage(null);
        tryUpdate.accept(drink, "Image can't be empty");
        drink.setImage("some-image-code");

        drink.setPrice(null);
        tryUpdate.accept(drink, "Price can't be null");
        drink.setPrice(-1);
        tryUpdate.accept(drink, "Price should be positive value");
        drink.setPrice(100);

        drink.setVolume(null);
        tryUpdate.accept(drink, "Volume can't be null");
        drink.setVolume(-1.0);
        tryUpdate.accept(drink, "Volume should be positive value");
    }

}
