package com.new_fast_pizza.controller;

import com.new_fast_pizza.Application;
import com.new_fast_pizza.component.AuthOperations;
import com.new_fast_pizza.model.ChangePasswordRequest;
import com.new_fast_pizza.model.LoginRequest;
import com.new_fast_pizza.model.SignUpClientRequest;
import com.new_fast_pizza.model.SignUpDriverRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.function.BiConsumer;

@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AuthControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private AuthOperations authOperations;

    @Value("${security.admin.username}")
    private String adminUsername;

    @Value("${security.admin.password}")
    private String adminPassword;

    @BeforeAll
    public void addUsers() {
        try {
            authOperations.signUpClient("test_client", "test_client");
            authOperations.signUpDriver("test_driver", "test_driver");
        } catch (Exception ignored) {
        }
    }

    @Test
    public void authorizeAsAdmin() {
        LoginRequest loginRequest = LoginRequest.builder()
                .username(adminUsername)
                .password(adminPassword)
                .build();
        ResponseEntity<String> responseEntity = testRestTemplate
                .postForEntity("/api/auth/signin", loginRequest, String.class);
        Assertions.assertEquals(200, responseEntity.getStatusCode().value());
        Assertions.assertNotNull(responseEntity.getBody());
    }

    @Test
    public void authorizeAsNotExistUser() {
        LoginRequest loginRequest = LoginRequest.builder()
                .username("notExistUser")
                .password("notExistUser")
                .build();
        ResponseEntity<String> responseEntity = testRestTemplate
                .postForEntity("/api/auth/signin", loginRequest, String.class);
        Assertions.assertEquals(401, responseEntity.getStatusCode().value());
    }

    @Test
    public void authorizeWithWrongPassword() {
        LoginRequest loginRequest = LoginRequest.builder()
                .username(adminUsername)
                .password(adminPassword + "wrong")
                .build();
        ResponseEntity<String> responseEntity = testRestTemplate
                .postForEntity("/api/auth/signin", loginRequest, String.class);
        Assertions.assertEquals(401, responseEntity.getStatusCode().value());
    }

    @Test
    public void registNewUser() {
        SignUpClientRequest signUpClientRequest = SignUpClientRequest.builder()
                .username("new_user")
                .name("New User")
                .email("newUser@gmail.com")
                .phone("8-(910)-034-76-36")
                .birthday(new GregorianCalendar(2000, Calendar.NOVEMBER, 11).getTime())
                .password("Qwerty123")
                .confirmPassword("Qwerty123")
                .build();
        ResponseEntity<String> signUpEntity = testRestTemplate
                .postForEntity("/api/auth/signup/client", signUpClientRequest, String.class);
        Assertions.assertEquals(200, signUpEntity.getStatusCode().value());
        Assertions.assertEquals("Client registered successfully!", signUpEntity.getBody());

        LoginRequest loginRequest = LoginRequest.builder()
                .username("new_user")
                .password("Qwerty123")
                .build();
        ResponseEntity<String> signInEntity = testRestTemplate.postForEntity("/api/auth/signin", loginRequest, String.class);
        Assertions.assertEquals(200, signInEntity.getStatusCode().value());
    }

    @Test
    public void checkValidationDuringRegistClient() {
        BiConsumer<SignUpClientRequest, String> tryRegist = (signRequest, message) -> {
            ResponseEntity<String> responseEntity = testRestTemplate
                    .postForEntity("/api/auth/signup/client", signRequest, String.class);
            Assertions.assertEquals(400, responseEntity.getStatusCode().value());
            Assertions.assertEquals(message, responseEntity.getBody());
        };
        SignUpClientRequest signUpClientRequest = SignUpClientRequest.builder()
                .name("Wrong User")
                .email("wrongUser@gmail.com")
                .phone("8-(910)-034-76-37")
                .birthday(new GregorianCalendar(2000, Calendar.NOVEMBER, 11).getTime())
                .password("Qwerty123")
                .confirmPassword("Qwerty123")
                .build();
        tryRegist.accept(signUpClientRequest, "Username can't be empty");
        signUpClientRequest.setUsername("wu");
        tryRegist.accept(signUpClientRequest, "Username should be minimum 3 symbols");
        signUpClientRequest.setUsername("Wrong User");
        tryRegist.accept(signUpClientRequest, "Username can consist of uppercase and lowercase latin letters, numbers or underscores");
        signUpClientRequest.setUsername("test_client");
        tryRegist.accept(signUpClientRequest, "User with username=test_client already exists");
        signUpClientRequest.setUsername("wrong_user");

        signUpClientRequest.setName(null);
        tryRegist.accept(signUpClientRequest, "Name can't be empty");
        signUpClientRequest.setName("Wrong Name89");
        tryRegist.accept(signUpClientRequest, "Name can consist of uppercase and lowercase latin letters, spaces or dashes");
        signUpClientRequest.setName("Wrong Name");

        signUpClientRequest.setEmail("empty-email");
        tryRegist.accept(signUpClientRequest, "Email should have email format");
        signUpClientRequest.setEmail("wrongUser@gmail.com");

        signUpClientRequest.setPhone(null);
        tryRegist.accept(signUpClientRequest, "Phone can't be empty");
        signUpClientRequest.setPhone("my phone");
        tryRegist.accept(signUpClientRequest, "Phone should have phone format");
        signUpClientRequest.setPhone("8-(910)-034-76-37");

        signUpClientRequest.setBirthday(new GregorianCalendar(2030, Calendar.NOVEMBER, 11).getTime());
        tryRegist.accept(signUpClientRequest, "Date should be past date");
        signUpClientRequest.setBirthday(new GregorianCalendar(2000, Calendar.NOVEMBER, 11).getTime());

        signUpClientRequest.setPassword(null);
        tryRegist.accept(signUpClientRequest, "Password can't be empty");
        signUpClientRequest.setPassword("1234");
        tryRegist.accept(signUpClientRequest, "Password should be minimum 5 symbols");
        signUpClientRequest.setPassword("Qwerty123©");
        tryRegist.accept(signUpClientRequest, "Password can consist of uppercase and lowercase latin letters or spec symbols");
        signUpClientRequest.setPassword("Qwerty123");

        signUpClientRequest.setConfirmPassword("12345");
        tryRegist.accept(signUpClientRequest, "Need to confirm the password");
    }

    @Test
    public void registDriver() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        SignUpDriverRequest signUpDriverRequest = SignUpDriverRequest.builder()
                .username("new_driver")
                .name("New Driver")
                .phone("8-920-457-46-24")
                .password("12345")
                .confirmPassword("12345")
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);
        ResponseEntity<String> signUpEntity = testRestTemplate.postForEntity("/api/auth/signup/driver",
                new HttpEntity<>(signUpDriverRequest, headers), String.class);
        Assertions.assertEquals(200, signUpEntity.getStatusCode().value());
        Assertions.assertEquals("Driver registered successfully!", signUpEntity.getBody());

        LoginRequest loginRequest = LoginRequest.builder()
                .username("new_driver")
                .password("12345")
                .build();
        ResponseEntity<String> signInEntity = testRestTemplate.postForEntity("/api/auth/signin", loginRequest, String.class);
        Assertions.assertEquals(200, signInEntity.getStatusCode().value());
    }

    @Test
    public void registDriverWithoutAuthorization() {
        SignUpDriverRequest signUpDriverRequest = SignUpDriverRequest.builder()
                .username("error_driver")
                .name("Error Driver")
                .phone("8-920-457-46-24")
                .password("12345")
                .confirmPassword("12345")
                .build();
        ResponseEntity<String> signUpResponse = testRestTemplate.postForEntity("/api/auth/signup/driver", signUpDriverRequest, String.class);
        Assertions.assertEquals(401, signUpResponse.getStatusCode().value());
    }

    @Test
    public void registDriverAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        SignUpDriverRequest signUpDriverRequest = SignUpDriverRequest.builder()
                .username("error_driver")
                .name("Error Driver")
                .phone("8-920-457-46-24")
                .password("12345")
                .confirmPassword("12345")
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);
        ResponseEntity<String> signUpEntity = testRestTemplate.postForEntity("/api/auth/signup/driver",
                new HttpEntity<>(signUpDriverRequest, headers), String.class);
        Assertions.assertEquals(403, signUpEntity.getStatusCode().value());
    }

    @Test
    public void registDriverAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        SignUpDriverRequest signUpDriverRequest = SignUpDriverRequest.builder()
                .username("error_driver")
                .name("Error Driver")
                .phone("8-920-457-46-24")
                .password("12345")
                .confirmPassword("12345")
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);
        ResponseEntity<String> signUpEntity = testRestTemplate.postForEntity("/api/auth/signup/driver",
                new HttpEntity<>(signUpDriverRequest, headers), String.class);
        Assertions.assertEquals(403, signUpEntity.getStatusCode().value());
    }

    @Test
    public void checkValidationDuringRegistDriver() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        BiConsumer<SignUpDriverRequest, String> tryRegist = (signRequest, message) -> {
            ResponseEntity<String> responseEntity = testRestTemplate
                    .postForEntity("/api/auth/signup/driver", new HttpEntity<>(signRequest, headers), String.class);
            Assertions.assertEquals(400, responseEntity.getStatusCode().value());
            Assertions.assertEquals(message, responseEntity.getBody());
        };

        SignUpDriverRequest signUpDriverRequest = SignUpDriverRequest.builder()
                .username("wrong_driver")
                .name("Wrong Driver")
                .phone("8-920-457-46-24")
                .password("12345")
                .confirmPassword("12345")
                .build();

        signUpDriverRequest.setUsername(null);
        tryRegist.accept(signUpDriverRequest, "Username can't be empty");
        signUpDriverRequest.setUsername("wd");
        tryRegist.accept(signUpDriverRequest, "Username should be minimum 3 symbols");
        signUpDriverRequest.setUsername("Wrong Driver");
        tryRegist.accept(signUpDriverRequest, "Username can consist of uppercase and lowercase latin letters, numbers or underscores");
        signUpDriverRequest.setUsername("test_driver");
        tryRegist.accept(signUpDriverRequest, "User with username=test_driver already exists");
        signUpDriverRequest.setUsername("wrong_driver");

        signUpDriverRequest.setName(null);
        tryRegist.accept(signUpDriverRequest, "Name can't be empty");
        signUpDriverRequest.setName("Wrong Driver89");
        tryRegist.accept(signUpDriverRequest, "Name can consist of uppercase and lowercase latin letters, spaces or dashes");
        signUpDriverRequest.setName("Wrong Driver");

        signUpDriverRequest.setPhone(null);
        tryRegist.accept(signUpDriverRequest, "Phone can't be empty");
        signUpDriverRequest.setPhone("Phone");
        tryRegist.accept(signUpDriverRequest, "Phone should have phone format");
        signUpDriverRequest.setPhone("8-920-457-46-24");

        signUpDriverRequest.setPassword(null);
        tryRegist.accept(signUpDriverRequest, "Password can't be empty");
        signUpDriverRequest.setPassword("1234");
        tryRegist.accept(signUpDriverRequest, "Password should be minimum 5 symbols");
        signUpDriverRequest.setPassword("12345Ё");
        tryRegist.accept(signUpDriverRequest, "Password can consist of uppercase and lowercase latin letters or spec symbols");
        signUpDriverRequest.setPassword("12345");

        signUpDriverRequest.setConfirmPassword("123456");
        tryRegist.accept(signUpDriverRequest, "Need to confirm the password");
    }

    @Test
    public void changeClientPassword() {
        authOperations.signUpClient("new_test_client", "new_test_client");
        String jwtToken = authOperations.login("new_test_client", "new_test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);
        ChangePasswordRequest changePasswordRequest = ChangePasswordRequest.builder()
                .oldPassword("new_test_client")
                .password("12345")
                .confirmPassword("12345")
                .build();
        ResponseEntity<String> changePasswordEntity = testRestTemplate.postForEntity("/api/auth/password",
                new HttpEntity<>(changePasswordRequest, headers), String.class);
        Assertions.assertEquals(200, changePasswordEntity.getStatusCode().value());
        Assertions.assertEquals("Password was changed successfully!", changePasswordEntity.getBody());

        LoginRequest loginRequest = LoginRequest.builder()
                .username("new_test_client")
                .password("12345")
                .build();
        ResponseEntity<String> loginEntity = testRestTemplate.postForEntity("/api/auth/signin", loginRequest, String.class);
        Assertions.assertEquals(200, loginEntity.getStatusCode().value());
        loginRequest.setPassword("new_test_client");
        loginEntity = testRestTemplate.postForEntity("/api/auth/signin", loginRequest, String.class);
        Assertions.assertEquals(401, loginEntity.getStatusCode().value());
    }

    @Test
    public void changeDriverPassword() {
        authOperations.signUpClient("new_test_driver", "new_test_driver");
        String jwtToken = authOperations.login("new_test_driver", "new_test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);
        ChangePasswordRequest changePasswordRequest = ChangePasswordRequest.builder()
                .oldPassword("new_test_driver")
                .password("12345")
                .confirmPassword("12345")
                .build();
        ResponseEntity<String> changePasswordEntity = testRestTemplate.postForEntity("/api/auth/password",
                new HttpEntity<>(changePasswordRequest, headers), String.class);
        Assertions.assertEquals(200, changePasswordEntity.getStatusCode().value());
        Assertions.assertEquals("Password was changed successfully!", changePasswordEntity.getBody());

        LoginRequest loginRequest = LoginRequest.builder()
                .username("new_test_driver")
                .password("12345")
                .build();
        ResponseEntity<String> loginEntity = testRestTemplate.postForEntity("/api/auth/signin", loginRequest, String.class);
        Assertions.assertEquals(200, loginEntity.getStatusCode().value());
        loginRequest.setPassword("new_test_driver");
        loginEntity = testRestTemplate.postForEntity("/api/auth/signin", loginRequest, String.class);
        Assertions.assertEquals(401, loginEntity.getStatusCode().value());
    }

    @Test
    public void changePasswordWithoutAuthorization() {
        ChangePasswordRequest changePasswordRequest = ChangePasswordRequest.builder()
                .oldPassword("new_test_driver")
                .password("12345")
                .confirmPassword("12345")
                .build();
        ResponseEntity<String> changePasswordEntity = testRestTemplate.postForEntity("/api/auth/password",
                new HttpEntity<>(changePasswordRequest), String.class);
        Assertions.assertEquals(401, changePasswordEntity.getStatusCode().value());
    }

    @Test
    public void changeAdminPassword() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);
        ChangePasswordRequest changePasswordRequest = ChangePasswordRequest.builder()
                .oldPassword(adminPassword)
                .password("12345")
                .confirmPassword("12345")
                .build();
        ResponseEntity<String> changePasswordEntity = testRestTemplate.postForEntity("/api/auth/password",
                new HttpEntity<>(changePasswordRequest, headers), String.class);
        Assertions.assertEquals(403, changePasswordEntity.getStatusCode().value());
    }

    @Test
    public void checkPasswordValidation() {
        authOperations.signUpClient("new_test_client_2", "new_test_client");
        String jwtToken = authOperations.login("new_test_client_2", "new_test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        BiConsumer<ChangePasswordRequest, String> tryChangePassword = (request, message) -> {
            ResponseEntity<String> changePasswordEntity = testRestTemplate.postForEntity("/api/auth/password",
                    new HttpEntity<>(request, headers), String.class);
            Assertions.assertEquals(400, changePasswordEntity.getStatusCode().value());
            Assertions.assertEquals(message, changePasswordEntity.getBody());
        };

        ChangePasswordRequest changePasswordRequest = ChangePasswordRequest.builder()
                .oldPassword("new_test_client")
                .password("12345")
                .confirmPassword("12345")
                .build();

        changePasswordRequest.setPassword(null);
        tryChangePassword.accept(changePasswordRequest, "Password can't be empty");
        changePasswordRequest.setPassword("1234");
        tryChangePassword.accept(changePasswordRequest, "Password should be minimum 5 symbols");
        changePasswordRequest.setPassword("12345Ё");
        tryChangePassword.accept(changePasswordRequest, "Password can consist of uppercase and lowercase latin letters or spec symbols");
        changePasswordRequest.setPassword("12345");

        changePasswordRequest.setOldPassword("Qwerty123");
        tryChangePassword.accept(changePasswordRequest, "Bad credentials");
        changePasswordRequest.setOldPassword("new_test_client");

        changePasswordRequest.setConfirmPassword("123456");
        tryChangePassword.accept(changePasswordRequest, "Need to confirm the password");
    }

}
