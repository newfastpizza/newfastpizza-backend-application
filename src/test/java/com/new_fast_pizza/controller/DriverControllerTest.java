package com.new_fast_pizza.controller;

import com.new_fast_pizza.Application;
import com.new_fast_pizza.component.AuthOperations;
import com.new_fast_pizza.component.TownOperations;
import com.new_fast_pizza.model.ChangeDriverProfileRequest;
import com.new_fast_pizza.model.entity.Driver;
import com.new_fast_pizza.model.entity.Order;
import com.new_fast_pizza.model.entity.TownPoint;
import com.new_fast_pizza.model.enums.DriverStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DriverControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private AuthOperations authOperations;

    @Autowired
    private TownOperations townOperations;

    @Value("${security.admin.username}")
    private String adminUsername;

    @Value("${security.admin.password}")
    private String adminPassword;

    @BeforeAll
    public void addUsers() {
        try {
            authOperations.signUpClient("test_client", "test_client");
            authOperations.signUpDriver("test_driver", "test_driver");
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getDriverProfile() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver> profileEntity = testRestTemplate.exchange("/api/drivers/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class);
        Assertions.assertEquals(200, profileEntity.getStatusCode().value());
        Driver driver = profileEntity.getBody();
        Assertions.assertNotNull(driver);
        Assertions.assertEquals("test_driver", driver.getUsername());
        Assertions.assertNull(driver.getPassword());
    }

    @Test
    public void getDriverProfileWithoutAuthorization() {
        ResponseEntity<String> profileEntity = testRestTemplate.getForEntity("/api/drivers/profile", String.class);
        Assertions.assertEquals(401, profileEntity.getStatusCode().value());
        System.out.println(profileEntity.getBody());
    }

    @Test
    public void getDriverProfileAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> profileEntity = testRestTemplate.exchange("/api/drivers/profile", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(403, profileEntity.getStatusCode().value());
    }

    @Test
    public void getDriverProfileAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> profileEntity = testRestTemplate.exchange("/api/drivers/profile", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(403, profileEntity.getStatusCode().value());
    }


    @Test
    public void getAllDrivers() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver[]> driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), Driver[].class);
        Assertions.assertEquals(200, driversEntity.getStatusCode().value());
        Driver[] driversArray = driversEntity.getBody();
        Assertions.assertNotNull(driversArray);
        List<Driver> drivers = Arrays.asList(driversArray);
        Assertions.assertNotEquals(0, drivers.size());
        Assertions.assertTrue(drivers.stream().anyMatch(driver -> driver.getUsername().equals("test_driver")));
    }

    @Test
    public void getAllClientsWithoutAuthorization() {
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<String> driversEntity = testRestTemplate.getForEntity("/api/drivers", String.class);
        Assertions.assertEquals(401, driversEntity.getStatusCode().value());
    }

    @Test
    public void getAllClientsWithClientAuthorization() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(403, driversEntity.getStatusCode().value());
    }

    @Test
    public void getAllClientsWithDriverAuthorization() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        Assertions.assertEquals(403, driversEntity.getStatusCode().value());
    }

    @Test
    public void getDriverById() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver> profileEntity = testRestTemplate.exchange("/api/drivers/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class);
        Driver driver = profileEntity.getBody();
        Assertions.assertNotNull(driver);
        Assertions.assertNotNull(driver.getId());

        ResponseEntity<Driver> driverByIdEntity = testRestTemplate.exchange("/api/drivers/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class, driver.getId());
        Assertions.assertEquals(200, driverByIdEntity.getStatusCode().value());
        Driver driverById = driverByIdEntity.getBody();
        Assertions.assertEquals(driver, driverById);

        driverByIdEntity = testRestTemplate.exchange("/api/drivers/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class, "not-exist-id");
        Assertions.assertEquals(403, driverByIdEntity.getStatusCode().value());
    }

    @Test
    public void getDriverByIdAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver[]> driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), Driver[].class);
        Driver[] driversArray = driversEntity.getBody();
        Assertions.assertNotNull(driversArray);
        List<Driver> drivers = Arrays.asList(driversArray);
        Driver driver = drivers.stream().filter(driver1 -> driver1.getUsername().equals("test_driver")).findAny().orElseThrow();

        ResponseEntity<Driver> driverByIdEntity = testRestTemplate.exchange("/api/drivers/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class, driver.getId());
        Assertions.assertEquals(200, driverByIdEntity.getStatusCode().value());
        Driver driverById = driverByIdEntity.getBody();
        Assertions.assertEquals(driver, driverById);

        driverByIdEntity = testRestTemplate.exchange("/api/drivers/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class, "not-exist-id");
        Assertions.assertEquals(404, driverByIdEntity.getStatusCode().value());
    }

    @Test
    public void getDriverByIdAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> driverByIdEntity = testRestTemplate.exchange("/api/drivers/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), String.class, "some-id");
        Assertions.assertEquals(403, driverByIdEntity.getStatusCode().value());
    }

    @Test
    public void getDriverByIdWithoutAuthorization() {
        ResponseEntity<String> driverByIdEntity = testRestTemplate.getForEntity("/api/drivers/{id}",
                String.class, "some-id");
        Assertions.assertEquals(401, driverByIdEntity.getStatusCode().value());
    }

    @Test
    public void changeDriverProfile() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver[]> driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), Driver[].class);
        Driver[] driversArray = driversEntity.getBody();
        Assertions.assertNotNull(driversArray);
        List<Driver> drivers = Arrays.asList(driversArray);
        Driver driver = drivers.stream().filter(driver1 -> driver1.getUsername().equals("test_driver")).findAny().orElseThrow();

        ChangeDriverProfileRequest changeDriverProfileRequest = ChangeDriverProfileRequest.builder()
                .name("Test Driver")
                .phone(driver.getPhone())
                .build();
        ResponseEntity<Driver> changeProfileEntity = testRestTemplate.postForEntity("/api/drivers/{id}",
                new HttpEntity<>(changeDriverProfileRequest, headers), Driver.class, driver.getId());
        Assertions.assertEquals(200, changeProfileEntity.getStatusCode().value());
        Driver newProfile = changeProfileEntity.getBody();
        Assertions.assertNotNull(newProfile);
        Assertions.assertNotEquals(driver, newProfile);
        driver.setName(newProfile.getName());
        Assertions.assertEquals(driver, newProfile);

        ResponseEntity<Driver> driverByIdEntity = testRestTemplate.exchange("/api/drivers/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class, newProfile.getId());
        Assertions.assertEquals(200, driverByIdEntity.getStatusCode().value());
        driver = driverByIdEntity.getBody();
        Assertions.assertNotNull(driver);
        Assertions.assertEquals(newProfile, driver);

        ResponseEntity<String> changeProfileEntityAgain = testRestTemplate.postForEntity("/api/drivers/{id}",
                new HttpEntity<>(changeDriverProfileRequest, headers), String.class, "some-id");
        Assertions.assertEquals(404, changeProfileEntityAgain.getStatusCode().value());
    }

    @Test
    public void changeDriverProfileAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver> profileEntity = testRestTemplate.exchange("/api/drivers/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class);
        Driver driver = profileEntity.getBody();
        Assertions.assertNotNull(driver);

        ChangeDriverProfileRequest changeDriverProfileRequest = ChangeDriverProfileRequest.builder()
                .name("Test Driver")
                .phone(driver.getPhone())
                .build();
        ResponseEntity<String> changeProfileEntity = testRestTemplate.postForEntity("/api/drivers/{id}}",
                new HttpEntity<>(changeDriverProfileRequest, headers), String.class, driver.getId());
        Assertions.assertEquals(403, changeProfileEntity.getStatusCode().value());
    }

    @Test
    public void changeDriverProfileForDriver() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ChangeDriverProfileRequest changeDriverProfileRequest = ChangeDriverProfileRequest.builder()
                .name("Test Driver")
                .phone("8-356-675-73-56")
                .build();
        ResponseEntity<String> changeProfileEntity = testRestTemplate.postForEntity("/api/drivers/{id}}",
                new HttpEntity<>(changeDriverProfileRequest, headers), String.class, "some-id");
        Assertions.assertEquals(403, changeProfileEntity.getStatusCode().value());
    }

    @Test
    public void changeDriverProfileWithoutAuthorization() {
        ChangeDriverProfileRequest changeDriverProfileRequest = ChangeDriverProfileRequest.builder()
                .name("Test Driver")
                .phone("8-356-675-73-56")
                .build();
        ResponseEntity<String> changeProfileEntity = testRestTemplate.postForEntity("/api/drivers/{id}}",
                changeDriverProfileRequest, String.class, "some-id");
        Assertions.assertEquals(401, changeProfileEntity.getStatusCode().value());
    }

    @Test
    public void checkChangeDriverProfileValidation() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver[]> driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), Driver[].class);
        Driver[] driversArray = driversEntity.getBody();
        Assertions.assertNotNull(driversArray);
        List<Driver> drivers = Arrays.asList(driversArray);
        Driver driver = drivers.stream().filter(driver1 -> driver1.getUsername().equals("test_driver")).findAny().orElseThrow();

        BiConsumer<ChangeDriverProfileRequest, String> tryChange = (request, message) -> {
            ResponseEntity<String> changeProfileEntity = testRestTemplate.postForEntity("/api/drivers/{id}",
                    new HttpEntity<>(request, headers), String.class, driver.getId());
            Assertions.assertEquals(400, changeProfileEntity.getStatusCode().value());
            Assertions.assertEquals(message, changeProfileEntity.getBody());
        };

        ChangeDriverProfileRequest changeDriverProfileRequest = ChangeDriverProfileRequest.builder()
                .name("Test Driver")
                .phone(driver.getPhone())
                .build();

        changeDriverProfileRequest.setName(null);
        tryChange.accept(changeDriverProfileRequest, "Name can't be empty");
        changeDriverProfileRequest.setName("Test Driver5");
        tryChange.accept(changeDriverProfileRequest, "Name can consist of uppercase and lowercase latin letters, spaces or dashes");
        changeDriverProfileRequest.setName("Test Driver");

        changeDriverProfileRequest.setPhone(null);
        tryChange.accept(changeDriverProfileRequest, "Phone can't be empty");
        changeDriverProfileRequest.setPhone("some phone");
        tryChange.accept(changeDriverProfileRequest, "Phone should have phone format");
    }

    @Test
    public void deleteDriver() {
        authOperations.signUpDriver("deleted_driver", "deleted_driver");
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver[]> driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), Driver[].class);
        Driver[] driversArray = driversEntity.getBody();
        Assertions.assertNotNull(driversArray);
        List<Driver> drivers = Arrays.asList(driversArray);
        Driver driver = drivers.stream().filter(driver1 -> driver1.getUsername().equals("deleted_driver")).findAny().orElseThrow();

        ResponseEntity<Driver> deleteProfileEntity = testRestTemplate.exchange("/api/drivers/{id}", HttpMethod.DELETE,
                new HttpEntity<>(headers), Driver.class, driver.getId());
        Assertions.assertEquals(200, deleteProfileEntity.getStatusCode().value());
        Driver deletedDriver = deleteProfileEntity.getBody();
        Assertions.assertNotNull(deletedDriver);
        Assertions.assertEquals("deleted_driver", deletedDriver.getUsername());

        driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), Driver[].class);
        Assertions.assertEquals(200, driversEntity.getStatusCode().value());
        driversArray = driversEntity.getBody();
        Assertions.assertNotNull(driversArray);
        drivers = Arrays.asList(driversArray);
        Assertions.assertFalse(drivers.stream().anyMatch(driver1 -> driver1.getUsername().equals("deleted_driver")));

        ResponseEntity<String> deleteProfileEntityAgain = testRestTemplate.exchange("/api/drivers/{id}", HttpMethod.DELETE,
                new HttpEntity<>(headers), String.class, "some-id");
        Assertions.assertEquals(404, deleteProfileEntityAgain.getStatusCode().value());
    }

    @Test
    public void deleteDriverAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> deleteProfileEntity = testRestTemplate.exchange("/api/drivers/{id}", HttpMethod.DELETE,
                new HttpEntity<>(headers), String.class, "some-id");
        Assertions.assertEquals(403, deleteProfileEntity.getStatusCode().value());
    }

    @Test
    public void deleteDriverAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> deleteProfileEntity = testRestTemplate.exchange("/api/drivers/{id}", HttpMethod.DELETE,
                new HttpEntity<>(headers), String.class, "some-id");
        Assertions.assertEquals(403, deleteProfileEntity.getStatusCode().value());
    }

    @Test
    public void deleteDriverWithoutAuthorization() {
        ResponseEntity<String> deleteProfileEntity = testRestTemplate.getForEntity("/api/drivers/{id}", String.class, "some-id");
        Assertions.assertEquals(401, deleteProfileEntity.getStatusCode().value());
    }

    @Test
    public void getPositionForDriverById() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver> profileEntity = testRestTemplate.exchange("/api/drivers/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class);
        Driver driver = profileEntity.getBody();
        Assertions.assertNotNull(driver);

        ResponseEntity<TownPoint> positionEntity = testRestTemplate.exchange("/api/drivers/{id}/position", HttpMethod.GET,
                new HttpEntity<>(headers), TownPoint.class, driver.getId());
        Assertions.assertEquals(200, positionEntity.getStatusCode().value());

        positionEntity = testRestTemplate.exchange("/api/drivers/{id}/position", HttpMethod.GET,
                new HttpEntity<>(headers), TownPoint.class, "some-id");
        Assertions.assertEquals(403, positionEntity.getStatusCode().value());
    }

    @Test
    public void getPositionForDriverByIdAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver[]> driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), Driver[].class);
        Driver[] driversArray = driversEntity.getBody();
        Assertions.assertNotNull(driversArray);
        List<Driver> drivers = Arrays.asList(driversArray);
        Driver driver = drivers.stream().filter(driver1 -> driver1.getUsername().equals("test_driver")).findAny().orElseThrow();

        ResponseEntity<TownPoint> positionEntity = testRestTemplate.exchange("/api/drivers/{id}/position", HttpMethod.GET,
                new HttpEntity<>(headers), TownPoint.class, driver.getId());
        Assertions.assertEquals(200, positionEntity.getStatusCode().value());

        positionEntity = testRestTemplate.exchange("/api/drivers/{id}/position", HttpMethod.GET,
                new HttpEntity<>(headers), TownPoint.class, "some-id");
        Assertions.assertEquals(404, positionEntity.getStatusCode().value());
    }

    @Test
    public void getPositionForDriverByIdAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<TownPoint> positionEntity = testRestTemplate.exchange("/api/drivers/{id}/position", HttpMethod.GET,
                new HttpEntity<>(headers), TownPoint.class, "some-id");
        Assertions.assertEquals(403, positionEntity.getStatusCode().value());
    }

    @Test
    public void getPositionForDriverByIdWithoutAuthorization() {
        ResponseEntity<TownPoint> positionEntity = testRestTemplate.getForEntity("/api/drivers/{id}/position",
                TownPoint.class, "some-id");
        Assertions.assertEquals(401, positionEntity.getStatusCode().value());
    }

    @Test
    public void updatePositionAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver> profileEntity = testRestTemplate.exchange("/api/drivers/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class);
        Driver driver = profileEntity.getBody();
        Assertions.assertNotNull(driver);

        TownPoint townPoint = townOperations.getTownByAddress("Gagarina", "1");
        ResponseEntity<TownPoint> positionEntity = testRestTemplate.postForEntity("/api/drivers/{id}/position",
                new HttpEntity<>(townPoint.getId(), headers), TownPoint.class, driver.getId());
        Assertions.assertEquals(200, positionEntity.getStatusCode().value());
        TownPoint newPosition = positionEntity.getBody();
        Assertions.assertNotNull(newPosition);
        Assertions.assertEquals(townPoint, newPosition);

        positionEntity = testRestTemplate.exchange("/api/drivers/{id}/position", HttpMethod.GET,
                new HttpEntity<>(headers), TownPoint.class, driver.getId());
        Assertions.assertEquals(200, positionEntity.getStatusCode().value());
        newPosition = positionEntity.getBody();
        Assertions.assertNotNull(newPosition);
        Assertions.assertEquals(townPoint, newPosition);

        positionEntity = testRestTemplate.postForEntity("/api/drivers/{id}/position",
                new HttpEntity<>(townPoint.getId(), headers), TownPoint.class, "some-id");
        Assertions.assertEquals(403, positionEntity.getStatusCode().value());
    }

    @Test
    public void updatePositionAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver[]> driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), Driver[].class);
        Driver[] driversArray = driversEntity.getBody();
        Assertions.assertNotNull(driversArray);
        List<Driver> drivers = Arrays.asList(driversArray);
        Driver driver = drivers.stream().filter(driver1 -> driver1.getUsername().equals("test_driver")).findAny().orElseThrow();

        TownPoint townPoint = townOperations.getTownByAddress("Gagarina", "1");

        ResponseEntity<TownPoint> positionEntity = testRestTemplate.postForEntity("/api/drivers/{id}/position",
                new HttpEntity<>(townPoint.getId(), headers), TownPoint.class, driver.getId());
        Assertions.assertEquals(200, positionEntity.getStatusCode().value());
        TownPoint newPosition = positionEntity.getBody();
        Assertions.assertNotNull(newPosition);
        Assertions.assertEquals(townPoint, newPosition);

        positionEntity = testRestTemplate.exchange("/api/drivers/{id}/position", HttpMethod.GET,
                new HttpEntity<>(headers), TownPoint.class, driver.getId());
        Assertions.assertEquals(200, positionEntity.getStatusCode().value());
        newPosition = positionEntity.getBody();
        Assertions.assertNotNull(newPosition);
        Assertions.assertEquals(townPoint, newPosition);

        positionEntity = testRestTemplate.postForEntity("/api/drivers/{id}/position",
                new HttpEntity<>(townPoint.getId(), headers), TownPoint.class, "some-id");
        Assertions.assertEquals(404, positionEntity.getStatusCode().value());
    }

    @Test
    public void updatePositionAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        TownPoint townPoint = townOperations.getTownByAddress("Gagarina", "1");
        ResponseEntity<TownPoint> positionEntity = testRestTemplate.postForEntity("/api/drivers/{id}/position",
                new HttpEntity<>(townPoint, headers), TownPoint.class, "some-id");
        Assertions.assertEquals(403, positionEntity.getStatusCode().value());
    }

    @Test
    public void updatePositionWithoutAuthorization() {
        TownPoint townPoint = townOperations.getTownByAddress("Gagarina", "1");
        ResponseEntity<TownPoint> positionEntity = testRestTemplate.postForEntity("/api/drivers/{id}/position",
                townPoint, TownPoint.class, "some-id");
        Assertions.assertEquals(401, positionEntity.getStatusCode().value());
    }

    @Test
    public void updateStatusAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver> profileEntity = testRestTemplate.exchange("/api/drivers/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class);
        Driver driver = profileEntity.getBody();
        Assertions.assertNotNull(driver);

        ResponseEntity<Driver> statusEntity = testRestTemplate.postForEntity("/api/drivers/{id}/status",
                new HttpEntity<>(DriverStatus.FREE, headers), Driver.class, driver.getId());
        Assertions.assertEquals(200, statusEntity.getStatusCode().value());
        Driver newStatusDriver = statusEntity.getBody();
        Assertions.assertNotNull(newStatusDriver);
        Assertions.assertNotEquals(driver, newStatusDriver);
        driver.setStatus(DriverStatus.FREE);
        Assertions.assertEquals(driver, newStatusDriver);

        profileEntity = testRestTemplate.exchange("/api/drivers/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class);
        driver = profileEntity.getBody();
        Assertions.assertNotNull(driver);
        Assertions.assertEquals(DriverStatus.FREE, driver.getStatus());

        testRestTemplate.postForEntity("/api/drivers/{id}/status",
                new HttpEntity<>(DriverStatus.OUT_OF_OFFICE, headers), Driver.class, driver.getId());

        statusEntity = testRestTemplate.postForEntity("/api/drivers/{id}/status",
                new HttpEntity<>(DriverStatus.FREE, headers), Driver.class, "some-id");
        Assertions.assertEquals(403, statusEntity.getStatusCode().value());
    }

    @Test
    public void updateStatusAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver[]> driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), Driver[].class);
        Driver[] driversArray = driversEntity.getBody();
        Assertions.assertNotNull(driversArray);
        List<Driver> drivers = Arrays.asList(driversArray);
        Driver driver = drivers.stream().filter(driver1 -> driver1.getUsername().equals("test_driver")).findAny().orElseThrow();

        ResponseEntity<Driver> statusEntity = testRestTemplate.postForEntity("/api/drivers/{id}/status",
                new HttpEntity<>(DriverStatus.FREE, headers), Driver.class, driver.getId());
        Assertions.assertEquals(200, statusEntity.getStatusCode().value());
        Driver newStatusDriver = statusEntity.getBody();
        Assertions.assertNotNull(newStatusDriver);
        Assertions.assertNotEquals(driver, newStatusDriver);
        driver.setStatus(DriverStatus.FREE);
        Assertions.assertEquals(driver, newStatusDriver);

        ResponseEntity<Driver> driverEntity = testRestTemplate.exchange("/api/drivers/{id}", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class, driver.getId());
        driver = driverEntity.getBody();
        Assertions.assertNotNull(driver);
        Assertions.assertEquals(DriverStatus.FREE, driver.getStatus());

        testRestTemplate.postForEntity("/api/drivers/{id}/status",
                new HttpEntity<>(DriverStatus.OUT_OF_OFFICE, headers), Driver.class, driver.getId());

        statusEntity = testRestTemplate.postForEntity("/api/drivers/{id}/status",
                new HttpEntity<>(DriverStatus.FREE, headers), Driver.class, "some-id");
        Assertions.assertEquals(404, statusEntity.getStatusCode().value());
    }

    @Test
    public void updateStatusAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> statusEntity = testRestTemplate.postForEntity("/api/drivers/{id}/status",
                new HttpEntity<>(DriverStatus.FREE, headers), String.class, "some-id");
        Assertions.assertEquals(403, statusEntity.getStatusCode().value());
    }

    @Test
    public void updateStatusWithoutAuthorization() {
        ResponseEntity<String> statusEntity = testRestTemplate.postForEntity("/api/drivers/{id}/status",
                DriverStatus.FREE, String.class, "some-id");
        Assertions.assertEquals(401, statusEntity.getStatusCode().value());
    }

    @Test
    public void getOrderAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver> profileEntity = testRestTemplate.exchange("/api/drivers/profile", HttpMethod.GET,
                new HttpEntity<>(headers), Driver.class);
        Driver driver = profileEntity.getBody();
        Assertions.assertNotNull(driver);

        ResponseEntity<Order> orderEntity = testRestTemplate.exchange("/api/drivers/{id}/order", HttpMethod.GET,
                new HttpEntity<>(headers), Order.class, driver.getId());
        Assertions.assertEquals(200, orderEntity.getStatusCode().value());

        orderEntity = testRestTemplate.exchange("/api/drivers/{id}/order", HttpMethod.GET,
                new HttpEntity<>(headers), Order.class, "some-id");
        Assertions.assertEquals(403, orderEntity.getStatusCode().value());
    }

    @Test
    public void getOrderAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<Driver[]> driversEntity = testRestTemplate.exchange("/api/drivers", HttpMethod.GET,
                new HttpEntity<>(headers), Driver[].class);
        Driver[] driversArray = driversEntity.getBody();
        Assertions.assertNotNull(driversArray);
        List<Driver> drivers = Arrays.asList(driversArray);
        Driver driver = drivers.stream().filter(driver1 -> driver1.getUsername().equals("test_driver")).findAny().orElseThrow();

        ResponseEntity<Order> orderEntity = testRestTemplate.exchange("/api/drivers/{id}/order", HttpMethod.GET,
                new HttpEntity<>(headers), Order.class, driver.getId());
        Assertions.assertEquals(200, orderEntity.getStatusCode().value());

        orderEntity = testRestTemplate.exchange("/api/drivers/{id}/order", HttpMethod.GET,
                new HttpEntity<>(headers), Order.class, "some-id");
        Assertions.assertEquals(404, orderEntity.getStatusCode().value());
    }

    @Test
    public void getOrderAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> orderEntity = testRestTemplate.exchange("/api/drivers/{id}/order", HttpMethod.GET,
                new HttpEntity<>(headers), String.class, "some-id");
        Assertions.assertEquals(403, orderEntity.getStatusCode().value());
    }

    @Test
    public void getOrderWithoutAuthorization() {
        ResponseEntity<String> orderEntity = testRestTemplate.getForEntity("/api/drivers/{id}/order",
                String.class, "some-id");
        Assertions.assertEquals(401, orderEntity.getStatusCode().value());
    }
}
