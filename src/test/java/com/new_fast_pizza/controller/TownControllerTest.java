package com.new_fast_pizza.controller;

import com.new_fast_pizza.Application;
import com.new_fast_pizza.component.AuthOperations;
import com.new_fast_pizza.model.PointsPath;
import com.new_fast_pizza.model.entity.TownPoint;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TownControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private AuthOperations authOperations;

    @Value("${security.admin.username}")
    private String adminUsername;

    @Value("${security.admin.password}")
    private String adminPassword;

    @BeforeAll
    public void addUsers() {
        try {
            authOperations.signUpClient("test_client", "test_client");
            authOperations.signUpDriver("test_driver", "test_driver");
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getAllTownPoints() {
        ResponseEntity<TownPoint[]> townEntity = testRestTemplate.getForEntity("/api/town", TownPoint[].class);
        Assertions.assertEquals(200, townEntity.getStatusCode().value());
        TownPoint[] townPointsArray = townEntity.getBody();
        Assertions.assertNotNull(townPointsArray);
        List<TownPoint> townPoints = Arrays.asList(townPointsArray);
        Assertions.assertEquals(96, townPoints.size());
    }

    @Test
    public void getAllOffices() {
        ResponseEntity<TownPoint[]> townEntity = testRestTemplate.getForEntity("/api/town/offices", TownPoint[].class);
        Assertions.assertEquals(200, townEntity.getStatusCode().value());
        TownPoint[] townPointsArray = townEntity.getBody();
        Assertions.assertNotNull(townPointsArray);
        List<TownPoint> townPoints = Arrays.asList(townPointsArray);
        Assertions.assertEquals(3, townPoints.size());
    }

    @Test
    public void getTownPointById() {
        ResponseEntity<TownPoint[]> townEntity = testRestTemplate.getForEntity("/api/town", TownPoint[].class);
        Assertions.assertEquals(200, townEntity.getStatusCode().value());
        TownPoint[] townPointsArray = townEntity.getBody();
        Assertions.assertNotNull(townPointsArray);
        List<TownPoint> townPoints = Arrays.asList(townPointsArray);
        TownPoint townPoint = townPoints.get(0);

        ResponseEntity<TownPoint> townPointEntity = testRestTemplate.getForEntity("/api/town/{id}",
                TownPoint.class, townPoint.getId());
        Assertions.assertEquals(200, townPointEntity.getStatusCode().value());
        TownPoint townPointById = townPointEntity.getBody();
        Assertions.assertNotNull(townPointById);
        Assertions.assertEquals(townPoint, townPointById);

        townPointEntity = testRestTemplate.getForEntity("/api/town/{id}", TownPoint.class, "some-id");
        Assertions.assertEquals(404, townPointEntity.getStatusCode().value());
    }

    @Test
    public void getTownPointByAddress() {
        ResponseEntity<TownPoint[]> townEntity = testRestTemplate.getForEntity("/api/town", TownPoint[].class);
        Assertions.assertEquals(200, townEntity.getStatusCode().value());
        TownPoint[] townPointsArray = townEntity.getBody();
        Assertions.assertNotNull(townPointsArray);
        List<TownPoint> townPoints = Arrays.asList(townPointsArray);
        TownPoint townPoint = townPoints.stream().filter(townPoint1 -> townPoint1.getStreet().equals("Gagarina") &&
                townPoint1.getBuilding().equals("1")).findAny().orElseThrow();

        ResponseEntity<TownPoint> townPointEntity = testRestTemplate.getForEntity("/api/town/{street}/{building}",
                TownPoint.class, "Gagarina", "1");
        Assertions.assertEquals(200, townPointEntity.getStatusCode().value());
        TownPoint townPointByAddress = townPointEntity.getBody();
        Assertions.assertNotNull(townPointByAddress);
        Assertions.assertEquals(townPoint, townPointByAddress);

        townPointEntity = testRestTemplate.getForEntity("/api/town/{street}/{building}", TownPoint.class,
                "some street", "1");
        Assertions.assertEquals(404, townPointEntity.getStatusCode().value());

        townPointEntity = testRestTemplate.getForEntity("/api/town/{street}/{building}", TownPoint.class,
                "Gagarina", "some building");
        Assertions.assertEquals(404, townPointEntity.getStatusCode().value());
    }

    @Test
    public void getPathAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<TownPoint[]> townEntity = testRestTemplate.getForEntity("/api/town", TownPoint[].class);
        Assertions.assertEquals(200, townEntity.getStatusCode().value());
        TownPoint[] townPointsArray = townEntity.getBody();
        Assertions.assertNotNull(townPointsArray);
        List<TownPoint> townPoints = Arrays.asList(townPointsArray);
        TownPoint townPoint1 = townPoints.stream().filter(townPoint -> townPoint.getStreet().equals("Gagarina") &&
                townPoint.getBuilding().equals("1")).findAny().orElseThrow();
        TownPoint townPoint2 = townPoints.stream().filter(townPoint -> townPoint.getStreet().equals("Pokrovskaya") &&
                townPoint.getBuilding().equals("1")).findAny().orElseThrow();

        ResponseEntity<PointsPath> pathEntity = testRestTemplate.exchange("/api/town/path/{source}/{target}", HttpMethod.GET,
                new HttpEntity<>(headers), PointsPath.class, townPoint1.getId(), townPoint2.getId());
        Assertions.assertEquals(200, pathEntity.getStatusCode().value());
        PointsPath path = pathEntity.getBody();
        Assertions.assertNotNull(path);
        Assertions.assertNotNull(path.getPath());
        Assertions.assertEquals(7, path.getPath().size());
        Assertions.assertNotNull(path.getDistances());
        Assertions.assertEquals(6, path.getDistances().stream().mapToInt(i -> i).sum());

        pathEntity = testRestTemplate.exchange("/api/town/path/{source}/{target}", HttpMethod.GET,
                new HttpEntity<>(headers), PointsPath.class, townPoint1.getId(), "some-id");
        Assertions.assertEquals(404, pathEntity.getStatusCode().value());

        pathEntity = testRestTemplate.exchange("/api/town/path/{source}/{target}", HttpMethod.GET,
                new HttpEntity<>(headers), PointsPath.class, "some-id", townPoint2.getId());
        Assertions.assertEquals(404, pathEntity.getStatusCode().value());
    }

    @Test
    public void getPathAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<TownPoint[]> townEntity = testRestTemplate.getForEntity("/api/town", TownPoint[].class);
        Assertions.assertEquals(200, townEntity.getStatusCode().value());
        TownPoint[] townPointsArray = townEntity.getBody();
        Assertions.assertNotNull(townPointsArray);
        List<TownPoint> townPoints = Arrays.asList(townPointsArray);
        TownPoint townPoint1 = townPoints.stream().filter(townPoint -> townPoint.getStreet().equals("Gagarina") &&
                townPoint.getBuilding().equals("1")).findAny().orElseThrow();
        TownPoint townPoint2 = townPoints.stream().filter(townPoint -> townPoint.getStreet().equals("Pokrovskaya") &&
                townPoint.getBuilding().equals("1")).findAny().orElseThrow();

        ResponseEntity<PointsPath> pathEntity = testRestTemplate.exchange("/api/town/path/{source}/{target}", HttpMethod.GET,
                new HttpEntity<>(headers), PointsPath.class, townPoint1.getId(), townPoint2.getId());
        Assertions.assertEquals(200, pathEntity.getStatusCode().value());
        PointsPath path = pathEntity.getBody();
        Assertions.assertNotNull(path);
        Assertions.assertNotNull(path.getPath());
        Assertions.assertEquals(7, path.getPath().size());
        Assertions.assertNotNull(path.getDistances());
        Assertions.assertEquals(6, path.getDistances().stream().mapToInt(i -> i).sum());

        pathEntity = testRestTemplate.exchange("/api/town/path/{source}/{target}", HttpMethod.GET,
                new HttpEntity<>(headers), PointsPath.class, townPoint1.getId(), "some-id");
        Assertions.assertEquals(404, pathEntity.getStatusCode().value());

        pathEntity = testRestTemplate.exchange("/api/town/path/{source}/{target}", HttpMethod.GET,
                new HttpEntity<>(headers), PointsPath.class, "some-id", townPoint2.getId());
        Assertions.assertEquals(404, pathEntity.getStatusCode().value());
    }

    @Test
    public void getPathAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> pathEntity = testRestTemplate.exchange("/api/town/path/{source}/{target}",
                HttpMethod.GET, new HttpEntity<>(headers), String.class, "some-id1", "some-id2");
        Assertions.assertEquals(403, pathEntity.getStatusCode().value());
    }

    @Test
    public void getPathWithoitAuthorization() {
        ResponseEntity<String> pathEntity = testRestTemplate.getForEntity("/api/town/path/{source}/{target}",
                String.class, "some-id1", "some-id2");
        Assertions.assertEquals(401, pathEntity.getStatusCode().value());
    }

    @Test
    public void setOfficeStatusAsAdmin() {
        String jwtToken = authOperations.login(adminUsername, adminPassword);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<TownPoint[]> townEntity = testRestTemplate.getForEntity("/api/town", TownPoint[].class);
        Assertions.assertEquals(200, townEntity.getStatusCode().value());
        TownPoint[] townPointsArray = townEntity.getBody();
        Assertions.assertNotNull(townPointsArray);
        List<TownPoint> townPoints = Arrays.asList(townPointsArray);
        TownPoint townPoint = townPoints.stream().filter(townPoint1 -> townPoint1.getStreet().equals("Gagarina") &&
                townPoint1.getBuilding().equals("1")).findAny().orElseThrow();

        ResponseEntity<TownPoint> setOfficeEntity = testRestTemplate.postForEntity("/api/town/{id}",
                new HttpEntity<>(true, headers), TownPoint.class, townPoint.getId());
        Assertions.assertEquals(200, setOfficeEntity.getStatusCode().value());
        TownPoint newOffice = setOfficeEntity.getBody();
        Assertions.assertNotNull(newOffice);
        Assertions.assertNotEquals(townPoint, newOffice);
        townPoint.setIsBranchOffice(true);
        Assertions.assertEquals(townPoint, newOffice);

        townEntity = testRestTemplate.getForEntity("/api/town/offices", TownPoint[].class);
        Assertions.assertEquals(200, townEntity.getStatusCode().value());
        townPointsArray = townEntity.getBody();
        Assertions.assertNotNull(townPointsArray);
        townPoints = Arrays.asList(townPointsArray);
        Assertions.assertEquals(4, townPoints.size());

        setOfficeEntity = testRestTemplate.postForEntity("/api/town/{id}",
                new HttpEntity<>(false, headers), TownPoint.class, townPoint.getId());
        Assertions.assertEquals(200, setOfficeEntity.getStatusCode().value());

        setOfficeEntity = testRestTemplate.postForEntity("/api/town/{id}",
                new HttpEntity<>(true, headers), TownPoint.class, "some-id");
        Assertions.assertEquals(404, setOfficeEntity.getStatusCode().value());
    }

    @Test
    public void setOfficeStatusAsDriver() {
        String jwtToken = authOperations.login("test_driver", "test_driver");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> setOfficeEntity = testRestTemplate.postForEntity("/api/town/{id}",
                new HttpEntity<>(true, headers), String.class, "some-id");
        Assertions.assertEquals(403, setOfficeEntity.getStatusCode().value());
    }

    @Test
    public void setOfficeStatusAsClient() {
        String jwtToken = authOperations.login("test_client", "test_client");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);

        ResponseEntity<String> setOfficeEntity = testRestTemplate.postForEntity("/api/town/{id}",
                new HttpEntity<>(true, headers), String.class, "some-id");
        Assertions.assertEquals(403, setOfficeEntity.getStatusCode().value());
    }

    @Test
    public void setOfficeStatusWithoutAuthorization() {
        ResponseEntity<String> setOfficeEntity = testRestTemplate.postForEntity("/api/town/{id}",
                true, String.class, "some-id");
        Assertions.assertEquals(401, setOfficeEntity.getStatusCode().value());
    }
}
