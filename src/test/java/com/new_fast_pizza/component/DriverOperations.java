package com.new_fast_pizza.component;

import com.new_fast_pizza.model.entity.Driver;
import com.new_fast_pizza.model.entity.TownPoint;
import com.new_fast_pizza.model.enums.DriverStatus;
import com.new_fast_pizza.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DriverOperations {

    @Autowired
    private DriverService driverService;

    public void setPosition(String driverUsername, TownPoint townPoint) {
        Driver driver = driverService.getAllDrivers().stream()
                .filter(driver1 -> driver1.getUsername().equals(driverUsername)).findAny().orElseThrow();
        driverService.updatePosition(driver.getId(), townPoint.getId());
    }

    public void setStatus(String driverUsername, DriverStatus driverStatus) {
        Driver driver = driverService.getAllDrivers().stream()
                .filter(driver1 -> driver1.getUsername().equals(driverUsername)).findAny().orElseThrow();
        driverService.updateStatus(driver.getId(), driverStatus);
    }
}
