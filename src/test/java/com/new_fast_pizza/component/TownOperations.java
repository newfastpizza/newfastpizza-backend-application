package com.new_fast_pizza.component;

import com.new_fast_pizza.model.entity.TownPoint;
import com.new_fast_pizza.service.TownService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TownOperations {

    @Autowired
    private TownService townService;

    public TownPoint getTownByAddress(String street, String building) {
        return townService.getPointByAddress(street, building);
    }
}
