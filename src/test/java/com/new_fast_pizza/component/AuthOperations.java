package com.new_fast_pizza.component;

import com.new_fast_pizza.model.entity.User;
import com.new_fast_pizza.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.GregorianCalendar;

@Component
public class AuthOperations {

    @Autowired
    private AuthService authService;

    @Value("${security.admin.username}")
    private String adminUsername;

    @Value("${security.admin.password}")
    private String adminPassword;

    public String login(String username, String password) {
        return authService.authenticateUser(username, password);
    }

    public void signUpClient(String username, String password) {
        authService.registClient(username, "User", username + "@gmail.com", "8-910-000-00-00",
                new GregorianCalendar(2000, Calendar.JANUARY, 1).getTime(), password);
    }

    public void signUpDriver(String username, String password) {
        authService.registDriver(username, "Driver", "8-920-000-00-00", password);
    }

    public User getCurrentUser() {
        return authService.getCurrentUser();
    }
}
