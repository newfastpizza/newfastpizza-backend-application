package com.new_fast_pizza.component;

import com.new_fast_pizza.model.entity.Drink;
import com.new_fast_pizza.model.entity.Pizza;
import com.new_fast_pizza.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductOperations {

    @Autowired
    private ProductService productService;

    public List<Pizza> getPizzas() {
        return productService.getAllPizzas();
    }

    public List<Drink> getDrinks() {
        return productService.getAllDrinks();
    }
}
