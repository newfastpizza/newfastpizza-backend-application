# newfastpizza-backend-application

1. Customize some parameters for your environment (db host, credentials, port etc.)
2. Run application using maven or IDE
3. You can examine api using swagger-ui (default http://localhost:8080/swagger-ui.html#)  
   P.S. Use host and port from application, if you changed them
   
### How to authorize swagger

1. Run application;
2. Go to swagger (default http://localhost:8080/swagger-ui.html#)
3. Press `auth-controller` -> `/api/auth/signin` -> `Try it out`
4. Enter credentials of user, that you want to authorize:  
    ![Enter credentials](./utils/screens/enter_credentials.PNG)  
5.  Press `Execute`
6. If user exists in the database, it returns 200 status code and jwt code:
    ![Copy jwt](./utils/screens/copy_jwt_code.PNG)
7. Press authorize button
    ![Authorize button](./utils/screens/authorize_button.PNG)
8. In opened window enter `Bearer ` and paste your jwt code. After that press `Authorize`:
    ![Login swagger](./utils/screens/log__in_swagger.PNG)
9. After that all your request will be sent with this authorization header:
    ![check_authorization](./utils/screens/check_authorize.PNG)
10. That's all:)