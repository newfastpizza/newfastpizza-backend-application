@startuml
hide empty methods
left to right direction

package REST_API <<Cloud>> {
}

package Backend <<Rectangle>> #B0C4DE {
  package component #FFFFFF {
    class StartupApplicationListener {
        -{static}Logger LOG
        -roleRepository: RoleRepository
        -userRepository: UserRepository
        -pizzaRepository: PizzaRepository
        -drinkRepository: DrinkRepository
        -townPointRepository: TownPointRepository
        -townLinkRepository: TownLinkRepository
        -encoder: PasswordEncoder
        -adminUsername: String
        -adminPassword: String
        -- Override Methods --
        +onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent)
        -loadRolesAndUsers()
        -loadProducts()
        -loadTown()
    }
    package security {
        class AuthEntryPointJwt {
            -{static}Logger LOG
            -- Override Methods --
            +commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
        }
        class AuthTokenFilter {
            -{static}Logger LOG
            -userService: UserService
            -jwtComponent: JwtComponent
            -- Override Methods --
            +doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
        }
        class JwtComponent {
            -{static}Logger LOG
            -jwtSecret: String
            -jwtExpirationMs: Integer
            -- Methods --
            +generateJwtToken(Authentication authentication): String
            +getUserNameFromJwtToken(String token): String
            +validateJwtToken(String authToken): Boolean
        }
        StartupApplicationListener -up[hidden]-> AuthEntryPointJwt
        AuthTokenFilter --> JwtComponent
    }
  }
  package configuration #FFFFFF {
        class WebSecurityConfig {
            -userService: UserService
            -unauthorizedHandler: AuthEntryPointJwt
            -authenticationJwtTokenFilter: AuthTokenFilter
            -- Methods --
            +passwordEncoder(): PasswordEncoder
            -- Override methods --
            +configure(AuthenticationManagerBuilder authenticationManagerBuilder)
            +authenticationManagerBean(): AuthenticationManager
            +configure(HttpSecurity http)
        }
        REST_API ..> WebSecurityConfig
        WebSecurityConfig --> AuthEntryPointJwt
        WebSecurityConfig --> AuthTokenFilter
  }
  package controller #FFFFFF {
    class AuthController {
        -authService: AuthService
        -- Methods --
        +authenticateUser(LoginRequest loginRequest): ResponseEntity<String>
        +registClient(SignUpClientRequest signUpClientRequest): ResponseEntity<String>
        +registDriver(SignUpDriverRequest signUpDriverRequest): ResponseEntity<String>
        +changePassword(ChangePasswordRequest changePasswordRequest): ResponseEntity<String>
    }
    class ClientController {
        -authService: AuthService
        -clientService: ClientService
        -- Methods --
        +getOwnProfile(): ResponseEntity<Client>
        +getClients(): ResponseEntity<List<Client>>
        +getClientById(String id): ResponseEntity<Client>
        +updateClientInfo(ChangeClientProfileRequest changeClientProfileRequest): ResponseEntity<Client>
        +deleteClient(): ResponseEntity<Client>
    }
    class DriverController {
        -driverService: DriverService
        -authService: AuthService
        -- Methods --
        +getOwnProfile(): ResponseEntity<Driver>
        +getDrivers(): ResponseEntity<List<Driver>>
        +getDriverById(String id): ResponseEntity<Driver>
        +updateDriverInfo(String id, ChangeDriverProfileRequest changeDriverProfileRequest): ResponseEntity<Driver>
        +deleteDriver(String id): ResponseEntity<Driver>
        +getOrderForDriverById(String id): ResponseEntity<Order>
        +getPositionForDriverById(String id): ResponseEntity<TownPoint>
        +updatePositionForDriverById(String id, String positionId): ResponseEntity<TownPoint>
        +updateStatusForDriverById(String id, DriverStatus status): ResponseEntity<Driver>
    }
    class OrderController {
        -authService: AuthService
        -orderService: OrderService
        -- Methods --
        +getOrders(): ResponseEntity<List<Order>>
        +createOrder(CreateOrderRequest createOrderRequest): ResponseEntity<Order>
        +getOrderById(String id): ResponseEntity<Order>
        +deleteOrderById(String id): ResponseEntity<Order>
        +finishOrder(String id): ResponseEntity<Order>
        +deliverOrder(String id): ResponseEntity<Order>
        +getClientForOrderById(String id): ResponseEntity<Client>
        +getDriverForOrderById(String id): ResponseEntity<Driver>
    }
    class ProductController {
        -productService: ProductService
        -- Methods --
        +getAllProducts(): ResponseEntity<List<Product>>
        +getAllPizzas(): ResponseEntity<List<Pizza>>
        +updatePizza(Pizza pizza): ResponseEntity<Pizza>
        +getAllDrinks(): ResponseEntity<List<Drink>>
        +updateDrink(Drink drink): ResponseEntity<Drink>
        +getProductById(String id): ResponseEntity<Product>
        +deleteProductById(String id): ResponseEntity<Product>
    }
    class TownController {
        -townService: TownService
        -- Methods --
        +getAllTownPoints(): ResponseEntity<List<TownPoint>>
        +getAllOffices(): ResponseEntity<List<TownPoint>>
        +getShortestPath(String sourceId, String targetId): ResponseEntity<PointsPath>
        +getTownPointById(String id): ResponseEntity<TownPoint>
        +setOfficeStatus(String id, Boolean isOffice): ResponseEntity<TownPoint>
    }
    REST_API --> AuthController
    REST_API --> ClientController
    REST_API --> DriverController
    REST_API --> OrderController
    REST_API --> ProductController
    REST_API --> TownController
  }
  package service #FFFFFF {
    hide fields
    interface AuthService {
        +authenticateUser(String username, String password): String
        +registClient(String username, String name, String email, String phone, Date birthday, String password)
        +registDriver(String username, String name, String phone, String password)
        +getCurrentUser(): User
        +changePassword(String oldPassword, String password)
    }
    interface ClientService {
        +getAllClients(): List<Client>
        +getClientById(String id): Client
        +updateClientInfo(String id, String name, String email, String phone): Client
        +disableClient(String id): Client
    }
    interface DriverService {
        +getAllDrivers(): List<Driver>
        +getDriverById(String id): Driver
        +updateDriverInfo(String id, String name, String phone): Driver
        +disableDriver(String id): Driver
        +updatePosition(String driverId, String townPointId): TownPoint
        +updateStatus(String driverId, DriverStatus status): Driver
        +getFreeDrivers(): List<Driver>
        +findDriver(Order order)
        +freeDriver(Driver driver)
    }
    interface OrderService {
        +getAllOrders(): List<Order>
        +getAllOrderByClient(String clientId): List<Order>
        +getAllOrderByDriver(String driverId): List<Order>
        +createOrder(Map<String, Integer> productsIds, String townPointId, Integer flatNumber, String phone, String comment, String clientId): Order
        +getOrderById(String id): Order
        +deleteOrderById(String id): Order
        +finishOrder(String id): Order
        +deliverOrder(String id): Order
    }
    interface ProductService {
        +getAllProducts(): List<Product>
        +getAllPizzas(): List<Pizza>
        +getAllDrinks(): List<Drink>
        +updatePizza(Pizza pizza): Pizza
        +updateDrink(Drink drink): Drink
        +getById(String id): Product
        +deleteById(String id): Product
    }
    interface TownService {
        +getNearestOffice(TownPoint townPoint): TownPoint
        +getAllTownPoints(): List<TownPoint>
        +getAllOffices(): List<TownPoint>
        +getPath(String sourceId, String targetId): PointsPath
        +getTownPointById(String id): TownPoint
        +setAsOffice(String id, Boolean isOffice): TownPoint
        +getPointByAddress(String street, String building): TownPoint
    }
    interface UserService {
        +loadUserByUsername(String username): User
        +getUserById(String id): User
    }
    AuthTokenFilter ---> UserService
    WebSecurityConfig ---> UserService
    AuthController ---> AuthService
    ClientController ---> AuthService
    ClientController ---> ClientService
    DriverController ---> DriverService
    DriverController ---> AuthService
    OrderController ---> AuthService
    OrderController ---> OrderService
    ProductController ---> ProductService
    TownController ---> TownService
    package impl as service_impl {
        class AuthServiceImpl {
            -{static}Logger LOG
            -authenticationManager: AuthenticationManager
            -userRepository: UserRepository
            -roleRepository: RoleRepository
            -encoder: PasswordEncoder
            -jwtComponent: JwtComponent
            -- Override Methods --
            +authenticateUser(String username, String password): String
            +registClient(String username, String name, String email, String phone, Date birthday, String password)
            +registDriver(String username, String name, String phone, String password)
            +getCurrentUser(): User
            +changePassword(String oldPassword, String password)
        }
        class ClientServiceImpl {
            -{static}Logger LOG
            -clientRepository: ClientRepository
            -- Override Methods --
            +getAllClients(): List<Client>
            +getClientById(String id): Client
            +updateClientInfo(String id, String name, String email, String phone): Client
            +disableClient(String id): Client
        }
        class DriverServiceImpl {
            -{static}Logger LOG
            -driverRepository: DriverRepository
            -townService: TownService
            -orderRepository: OrderRepository
            -- Methods --
            -findDriverTransaction(Order order): boolean
            -- Override Methods --
            +getAllDrivers(): List<Driver>
            +getDriverById(String id): Driver
            +updateDriverInfo(String id, String name, String phone): Driver
            +disableDriver(String id): Driver
            +updatePosition(String driverId, String townPointId): TownPoint
            +updateStatus(String driverId, DriverStatus status): Driver
            +getFreeDrivers(): List<Driver>
            +findDriver(Order order)
            +freeDriver(Driver driver)
        }
        class OrderServiceImpl {
            -{static}Logger LOG
            -orderRepository: OrderRepository
            -clientService: ClientService
            -driverService: DriverService
            -productService: ProductService
            -townService: TownService
            -- Override Methods --
            +getAllOrders(): List<Order>
            +getAllOrderByClient(String clientId): List<Order>
            +getAllOrderByDriver(String driverId): List<Order>
            +createOrder(Map<String, Integer> productsIds, String townPointId, Integer flatNumber, String phone, String comment, String clientId): Order
            +getOrderById(String id): Order
            +deleteOrderById(String id): Order
            +finishOrder(String id): Order
            +deliverOrder(String id): Order
        }
        class ProductServiceImpl {
            -{static}Logger LOG
            -pizzaRepository: PizzaRepository
            -drinkRepository: DrinkRepository
            -- Override Methods --
            +getAllProducts(): List<Product>
            +getAllPizzas(): List<Pizza>
            +getAllDrinks(): List<Drink>
            +updatePizza(Pizza pizza): Pizza
            +updateDrink(Drink drink): Drink
            +getById(String id): Product
            +deleteById(String id): Product
        }
        class TownServiceImpl {
            -{static}Logger LOG
            -townPointRepository: TownPointRepository
            -townLinkRepository: TownLinkRepository
            -- Override Methods --
            +getNearestOffice(TownPoint townPoint): TownPoint
            +getAllTownPoints(): List<TownPoint>
            +getAllOffices(): List<TownPoint>
            +getPath(String sourceId, String targetId): PointsPath
            +getTownPointById(String id): TownPoint
            +setAsOffice(String id, Boolean isOffice): TownPoint
            +getPointByAddress(String street, String building): TownPoint
        }
        class UserServiceImpl {
            -{static}Logger LOG
            -userRepository: UserRepository
            -- Override Methods --
            +loadUserByUsername(String username): User
            +getUserById(String id): User
        }
        AuthService <|.. AuthServiceImpl
        ClientService <|.. ClientServiceImpl
        DriverService <|.. DriverServiceImpl
        TownService <-- DriverServiceImpl
        OrderService <|.. OrderServiceImpl
        ClientService <-- OrderServiceImpl
        DriverService <-- OrderServiceImpl
        TownService <-- OrderServiceImpl
        ProductService <-- OrderServiceImpl
        ProductService <|.. ProductServiceImpl
        TownService <|.. TownServiceImpl
        UserService <|.. UserServiceImpl
    }
  }
  package repository #FFFFFF {
    hide fields
    interface ClientRepository {
        +findByIsEnabledTrue(): List<Client>
        +findByIdAndIsEnabledTrue(String id): Optional<Client>
        -- Override Methods --
        +findAll(): List<Client>
        +findById(String id): Optional<Client>
        +save(Client client): Client
    }
    interface DrinkRepository {
        +findByIsEnabledTrue(): List<Drink>
        +findByIdAndIsEnabledTrue(String id): Optional<Drink>
        -- Override Methods --
        +findAll(): List<Drink>
        +findById(String id): Optional<Drink>
        +save(Drink drink): Drink
    }
    interface DriverRepository {
        +findByIsEnabledTrue(): List<Driver>
        +findByStatus(DriverStatus driverStatus): List<Driver>
        +findByIdAndIsEnabledTrue(String id): Optional<Driver>
        -- Override Methods --
        +findAll(): List<Driver>
        +findById(String id): Optional<Driver>
        +save(Driver driver): Driver
    }
    interface OrderRepository {
        +findByClientId(String id): List<Order>
        +findByDriverId(String id): List<Driver>
        -- Override Methods --
        +findAll(): List<Order>
        +findById(String id): Optional<Order>
        +save(Order order): Order
        +delete(Order order)
    }
    interface PizzaRepository {
        +findByIsEnabledTrue(): List<Pizza>
        +findByIdAndIsEnabledTrue(String id): Optional<Pizza>
        -- Override Methods --
        +findAll(): List<Pizza>
        +findById(String id): Optional<Pizza>
        +save(Pizza pizza): Pizza
    }
    interface RoleRepository {
        +findRoleByRoleType(RoleType roleType): Optional<Role>
        +existsByRoleType(RoleType roleType): Boolean
        -- Override Methods --
        +findAll(): List<Role>
        +findById(String id): Optional<Role>
        +save(Role role): Role
    }
    interface TownLinkRepository {
        -- Override Methods --
        +findAll(): List<TownLink>
        +findById(String id): Optional<TownLink>
        +save(TownLink townLink): TownLink
    }
    interface TownPointRepository {
        +findAllByIsBranchOfficeTrue(): List<TownPoint>
        +findByStreetAndBuilding(String street, String building): Optional<TownPoint>
        -- Override Methods --
        +findAll(): List<TownPoint>
        +findById(String id): Optional<TownPoint>
        +save(TownPoint townPoint): TownPoint
    }
    interface UserRepository {
        +findByUsername(String username): Optional<User>
        +findByIdAndIsEnabledTrue(String id): Optional<User>
        +existsByUsername(String username): Boolean
        -- Override Methods --
        +findAll(): List<User>
        +findById(String id): Optional<User>
        +save(User user): User
    }
    StartupApplicationListener -----> UserRepository
    StartupApplicationListener -----> RoleRepository
    StartupApplicationListener -----> PizzaRepository
    StartupApplicationListener -----> DrinkRepository
    StartupApplicationListener -----> TownPointRepository
    StartupApplicationListener -----> TownLinkRepository
    AuthServiceImpl ----> UserRepository
    AuthServiceImpl ----> RoleRepository
    ClientServiceImpl ----> ClientRepository
    DriverServiceImpl ----> DriverRepository
    DriverServiceImpl ----> OrderRepository
    OrderServiceImpl ----> OrderRepository
    ProductServiceImpl ----> PizzaRepository
    ProductServiceImpl ----> DrinkRepository
    TownServiceImpl ----> TownPointRepository
    TownServiceImpl ----> TownLinkRepository
    UserServiceImpl --------> UserRepository
  }
  node Spring #FFFFFF {
    () ApplicationListener
    () OncePerRequestFilter
    StartupApplicationListener ...|> ApplicationListener
    AuthTokenFilter ...|> OncePerRequestFilter
  }
  node SpringSecurity #FFFFFF {
    () AuthenticationEntryPoint
    () WebSecurityConfigurerAdapter
    () UserDetailsService
    AuthEntryPointJwt ..|> AuthenticationEntryPoint
    WebSecurityConfig ..|> WebSecurityConfigurerAdapter
    UserService ..|> UserDetailsService
  }
  node Hibernate #FFFFFF {
    () JpaRepository
    ClientRepository ..|> JpaRepository: "<Client, String>"
    DrinkRepository ..|> JpaRepository: "<Drink, String>"
    DriverRepository ..|> JpaRepository: "<Driver, String>"
    OrderRepository ..|> JpaRepository: "<Order, String>"
    PizzaRepository ..|> JpaRepository: "<Pizza, String>"
    RoleRepository ..|> JpaRepository: "<Role, String>"
    TownLinkRepository ..|> JpaRepository: "<TownLink, String>"
    TownPointRepository ..|> JpaRepository: "<TownPoint, String>"
    UserRepository ..|> JpaRepository: "<User, String>"
  }
}

@enduml